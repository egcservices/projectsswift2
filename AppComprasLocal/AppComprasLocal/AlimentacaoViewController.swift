//
//  AlimentacaoViewController.swift
//  AppComprasLocal
//
//  Created by Bruno Rocha on 07/07/16.
//  Copyright (c) 2016 Bruno Rocha. All rights reserved.
//

import UIKit

class AlimentacaoViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {

    @IBOutlet weak var txtPesquisaAlim:UITextField!
    @IBOutlet weak var btnPesquisaAlim:UIButton!
    @IBOutlet weak var segmentTab:UISegmentedControl!
    @IBOutlet weak var tableViewAlm:UITableView!
    //@IBOutlet weak var btnTab:UITabBar!
    //@IBOutlet weak var btnTabDest:UITabBarItem!
    //@IBOutlet weak var btnTabTodos:UITabBarItem!
    var keysAlimGr : NSMutableArray! = NSMutableArray()
    var alimGroups : NSDictionary! = NSDictionary()
    var empresa : Empresa = Empresa()
    //var oldTab : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //Inicia o tab item desejado.
        //btnTab.selectedItem = btnTabDest as UITabBarItem
        let filepath = NSBundle.mainBundle().pathForResource("destalm", ofType: "plist")!
        alimGroups = NSDictionary(contentsOfFile: filepath)!
        keysAlimGr = NSMutableArray(array: alimGroups.allKeys)
        keysAlimGr.sortUsingSelector(NSSelectorFromString("compare:"))
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return keysAlimGr.count
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int)->Int
    {
//        let myKey:String = keysAlimGr.objectAtIndex(section) as! String
//        let secList: NSArray! = alimGroups.objectForKey(myKey) as! NSArray
//        var returnValue = 0
//        returnValue = secList.count
        
        
        let myKey:String = keysAlimGr.objectAtIndex(section) as! String
        let secList: NSArray! = alimGroups.objectForKey(myKey) as! NSArray
        return secList.count
        //return returnValue
    }
    
    /*
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
    let myCell = tableView.dequeueReusableCellWithIdentifier("myCell", forIndexPath: indexPath) as! UITableViewCell
*/
    
    //Cria os itens na lista.
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
       let cell = tableView.dequeueReusableCellWithIdentifier("myCell", forIndexPath: indexPath) as! UITableViewCell

        //var cell = tableView.dequeueReusableCellWithIdentifier("myCell") as! UITableViewCell!
        
        let section = indexPath.section
        let row = indexPath.row
        let key = keysAlimGr.objectAtIndex(section) as! String
        let groups = alimGroups.objectForKey(key) as! NSArray
        let alimGr = groups.objectAtIndex(row) as! String
        cell.textLabel!.text = alimGr
        
//        if (cell == nil){
//            cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "myCell")
//        }
        
        /*switch(segmentTab.selectedSegmentIndex){
        case 0 :
           let section = indexPath.section
            let row = indexPath.row
            let key = keysAlimGr.objectAtIndex(section) as! String
            let groups = alimGroups.objectForKey(key) as! NSArray
            let alimGr = groups.objectAtIndex(row) as! String
            cell.textLabel?.text = alimGr
            break

        case 1:
           let section = indexPath.section
            let row = indexPath.row
            let key = keysAlimGr.objectAtIndex(section) as! String
            let groups = alimGroups.objectForKey(key) as! NSArray
            let alimGr = groups.objectAtIndex(row) as! String
            cell.textLabel?.text = alimGr
            break
        default:
            break
        }*/
        
        return cell
        //        var cell : UITableViewCell! = tableView.dequeueReusableCellWithIdentifier("id") as! UITableViewCell!
//        
//        if (cell == nil){
//            cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "id")
//        }
//        
//        let section : Int = indexPath.section
//        let row: Int = indexPath.row
//        
//        let key : String = keysAlimGr.objectAtIndex(section) as! String
//        let groups : NSArray = alimGroups.objectForKey(key) as! NSArray
//        let alimGr : String = groups.objectAtIndex(row) as! String
//        
//        cell.textLabel?.text = alimGr
//        return cell
    }
    
    @IBAction func segmentedControlAction(sender:AnyObject)
    {
        self.tableViewAlm.reloadData()
        
        self.keysAlimGr = NSMutableArray()
        self.alimGroups = NSDictionary()

        if (self.segmentTab.selectedSegmentIndex == 0){
//            let filepath = NSBundle.mainBundle().pathForResource("destalm", ofType: "plist")!
//            alimGroups = NSDictionary(contentsOfFile: filepath)!
//            keysAlimGr = NSMutableArray(array: alimGroups.allKeys)
//            keysAlimGr.sortUsingSelector(NSSelectorFromString("compare:"))
            println("0")
        }else if (self.segmentTab.selectedSegmentIndex == 1){
//            let filepath = NSBundle.mainBundle().pathForResource("todosalm", ofType: "plist")!
//            alimGroups = NSDictionary(contentsOfFile: filepath)!
//            keysAlimGr = NSMutableArray(array: alimGroups.allKeys)
//            keysAlimGr.sortUsingSelector(NSSelectorFromString("compare:"))
            println("1")
        }
    }
    
    
//    func tabBar(tabBar: UITabBar, didSelectItem item: UITabBarItem) {
//        //This method will be called when user changes tab.
//        if (btnTab.selectedItem == btnTabDest as UITabBarItem){
//            let filepath = NSBundle.mainBundle().pathForResource("destalm", ofType: "plist")!
//            alimGroups = NSDictionary(contentsOfFile: filepath)!
//            keysAlimGr = NSMutableArray(array: alimGroups.allKeys)
//            keysAlimGr.sortUsingSelector(NSSelectorFromString("compare:"))
//            println("tab dest.. \(btnTabDest)")
//        }else if(btnTab.selectedItem == btnTabTodos as UITabBarItem){
//            let filepath = NSBundle.mainBundle().pathForResource("todosalm", ofType: "plist")!
//            alimGroups = NSDictionary(contentsOfFile: filepath)!
//            keysAlimGr = NSMutableArray(array: alimGroups.allKeys)
//            keysAlimGr.sortUsingSelector(NSSelectorFromString("compare:"))
//            println("tab todos.. \(btnTabTodos)")
//        }
//        
//        self.tableViewOpcoes.delegate = self
//    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
