//
//  GuiaViewController.swift
//  AppComprasLocal
//
//  Created by Bruno Rocha on 07/07/16.
//  Copyright (c) 2016 Bruno Rocha. All rights reserved.
//

import UIKit

class GuiaViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {

    var keysGuiasGr : NSMutableArray = NSMutableArray()
    var guiasGroups : NSDictionary = NSDictionary()
    
    override func viewDidLoad() {
        super.viewDidLoad()        // Do any additional setup after loading the view.
        let filepath = NSBundle.mainBundle().pathForResource("guia", ofType: "plist")!
        
        guiasGroups = NSDictionary(contentsOfFile: filepath)!
        keysGuiasGr = NSMutableArray(array: guiasGroups.allKeys)
        keysGuiasGr.sortUsingSelector(NSSelectorFromString("compare:"))

    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int)->Int
    {
        let myKey:String = keysGuiasGr.objectAtIndex(section) as! String
        let secList: NSArray = guiasGroups.objectForKey(myKey) as! NSArray
        return secList.count
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return keysGuiasGr.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let myId:String = "id"
        
        var cell : UITableViewCell! = tableView.dequeueReusableCellWithIdentifier(myId) as! UITableViewCell!
        
        if (cell == nil){
            cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: myId)
        }
        
        let section : Int = indexPath.section
        let row: Int = indexPath.row
        
        let key : String = keysGuiasGr.objectAtIndex(section) as! String
        let groups : NSArray = guiasGroups.objectForKey(key) as! NSArray
        let alimGr : String = groups.objectAtIndex(row) as! String
        
        cell.textLabel?.text = alimGr
        
        return cell
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
