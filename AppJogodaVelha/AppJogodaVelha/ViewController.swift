//
//  ViewController.swift
//  AppJogodaVelha
//
//  Created by Bruno Rocha on 29/07/16.
//  Copyright © 2016 Bruno Rocha. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet var button:UIButton!
    @IBOutlet var btnGameOver:UIButton!
    
    var jogador = 1
    var image = UIImage()
    var game = [0, 0, 0, 0, 0, 0, 0, 0, 0]
    var winCombination = [[0,1,2], [3,4,5], [6,7,8], [0,3,6], [1,4,7], [2,5,8], [0,4,8], [2,4,6]]
    var isGameOn = true
    var qtdCliks = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func jogar(sender: AnyObject){
        if game[sender.tag] == 0 && isGameOn{
            qtdCliks++
            game[sender.tag] = jogador
            
            if jogador == 1 {
                image = UIImage(named: "circle.png")!
                jogador = 2
            } else {
                image = UIImage(named: "cross.png")!
                jogador = 1
            }
        
            sender.setImage(image, forState: .Normal)
            
            for comb in winCombination{
                if game[comb[0]] != 0 && game[comb[0]] == game[comb[1]] && game[comb[1]] == game[comb[2]]{
                    isGameOn = false
                    btnGameOver.hidden = false
                    btnGameOver.setTitle("Jogador \(game[sender.tag]) ganhou, jogue novamente!", forState: .Normal)
                    UIView.animateWithDuration(0.5, animations: { () -> Void in
                        self.btnGameOver.center = CGPointMake(self.btnGameOver.center.x + 500, self.btnGameOver.center.y)
                    })
                    break
                }
            }
            
            if (qtdCliks == 9 && isGameOn){
                isGameOn = false
                btnGameOver.hidden = false
                btnGameOver.setTitle("Não houve ganhadores, jogue novamente!", forState: .Normal)
                UIView.animateWithDuration(0.5, animations: { () -> Void in
                    self.btnGameOver.center = CGPointMake(self.btnGameOver.center.x + 500, self.btnGameOver.center.y)
                })
            }
        }
    }
    
    @IBAction func playAgain(sender:AnyObject){
        btnGameOver.setTitle("Preparando para iniciar jogo...", forState: .Disabled)
        
        jogador = 1
        game = [0,0,0,0,0,0,0,0,0]
        
        isGameOn = true
        btnGameOver.hidden = true
        
        qtdCliks = 0
        
        var btn : UIButton
        for x in 0 ... 8 {
            btn = view.viewWithTag(x) as! UIButton!
            btn.setImage(nil, forState: .Normal)
        }
        
        UIView.animateWithDuration(0.5, animations: { () -> Void in
            self.btnGameOver.center = CGPointMake(self.btnGameOver.center.x - 500, self.btnGameOver.center.y)
        })
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}