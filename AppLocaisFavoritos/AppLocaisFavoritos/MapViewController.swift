//
//  ViewController.swift
//  AppLocaisFavoritos
//
//  Created by Bruno Rocha on 02/08/16.
//  Copyright © 2016 Bruno Rocha. All rights reserved.
//

import UIKit
import MapKit

class MapViewController: UIViewController, CLLocationManagerDelegate {
    
    @IBOutlet var map : MKMapView!
    
    var manager : CLLocationManager!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        manager = CLLocationManager()
        manager.delegate = self
        manager.desiredAccuracy = kCLLocationAccuracyBest
        
        if activePlace == -1{
            manager.requestWhenInUseAuthorization()
            manager.startUpdatingLocation()
        }else{
            let latitude = NSString(string: places[activePlace]["lat"]!).doubleValue
            let longitude = NSString(string: places[activePlace]["lon"]!).doubleValue
            
            let coordinate = CLLocationCoordinate2DMake(latitude, longitude)
            
            let latDel : CLLocationDegrees = 0.01
            let lonDel : CLLocationDegrees = 0.01
            
            let span : MKCoordinateSpan = MKCoordinateSpanMake(latDel, lonDel)
            
            let region : MKCoordinateRegion = MKCoordinateRegionMake(coordinate, span)
            
            self.map.setRegion(region, animated: true)
            
            let anotation = MKPointAnnotation()
            anotation.coordinate = coordinate
            anotation.title = places[activePlace]["name"]
            
            self.map.addAnnotation(anotation)

        }
        
        let uilpgr = UILongPressGestureRecognizer(target: self, action: "action:") //UILongPressGestureRecognizer
        uilpgr.minimumPressDuration = 2
        
        map.addGestureRecognizer(uilpgr)
    }
    
    func action(gestureRecognizer:UIGestureRecognizer){
        if gestureRecognizer.state == UIGestureRecognizerState.Began{
            let touchPoint = gestureRecognizer.locationInView(self.map)
            let newCoordinate = self.map.convertPoint(touchPoint, toCoordinateFromView: self.map)
            
            let location = CLLocation(latitude: newCoordinate.latitude, longitude: newCoordinate.longitude)
            CLGeocoder().reverseGeocodeLocation(location, completionHandler: { (placemarks, error) -> Void in
                var title = ""
                
                if error == nil{
                    if let p = placemarks?[0]{
                        var adress = ""
                        var adress2 = ""
                        
                        if p.subThoroughfare != nil{
                            adress2 = p.subThoroughfare!
                        }
                        
                        if p.thoroughfare != nil{
                            adress = p.thoroughfare!
                        }
                        
                        title = "\(adress2) \(adress)"
                    }
                    places.append(["name":title, "lat":"\(newCoordinate.latitude)", "lon":"\(newCoordinate.longitude)"])
                }
                
                if title.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet()) == ""{
                    title = "Adicionado em \(NSDate())"
                }
                
                let anotation = MKPointAnnotation()
                anotation.coordinate = newCoordinate
                anotation.title = "Novo local"
                
                self.map.addAnnotation(anotation)
            })
            
        }
    }

    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation : CLLocation = locations[0]

        let lat = userLocation.coordinate.latitude
        let long = userLocation.coordinate.longitude
        
        let coordinate = CLLocationCoordinate2DMake(lat, long)
        
        let latDel : CLLocationDegrees = 0.01
        let lonDel : CLLocationDegrees = 0.01
        
        let span : MKCoordinateSpan = MKCoordinateSpanMake(latDel, lonDel)
        
        let region : MKCoordinateRegion = MKCoordinateRegionMake(coordinate, span)
        
        self.map.setRegion(region, animated: true)
    
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}