//
//  ViewController.swift
//  AppMapa
//
//  Created by Bruno Rocha on 02/08/16.
//  Copyright © 2016 Bruno Rocha. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class ViewController: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate {

    @IBOutlet weak var map: MKMapView!
    var locManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        locManager.delegate = self
        locManager.desiredAccuracy = kCLLocationAccuracyBest
        locManager.requestWhenInUseAuthorization()
        locManager.startUpdatingLocation()
        
        //-8.1164794,-35.3265763
        //-8.1189039,-35.2966552
        let lat : CLLocationDegrees = -8.1189039
        let long : CLLocationDegrees = -35.2966552
        
        let latDel : CLLocationDegrees = 0.01
        let longDel : CLLocationDegrees = 0.01
        
        let span : MKCoordinateSpan = MKCoordinateSpanMake(latDel, longDel)
        let local : CLLocationCoordinate2D = CLLocationCoordinate2DMake(lat, long)
        
        let region : MKCoordinateRegion = MKCoordinateRegionMake(local, span)
        
        map.setRegion(region, animated: true)
        
        let anot = MKPointAnnotation()
        anot.coordinate = local
        anot.title = "Americanas"
        anot.subtitle = "Comércio"
        
        map.addAnnotation(anot)
        
        let pressedGesture = UILongPressGestureRecognizer(target: self, action: "action:")
        pressedGesture.minimumPressDuration = 2
        
        map.addGestureRecognizer(pressedGesture)
    }
    
    func action(gestureRecognizer : UIGestureRecognizer){
        let touch  = gestureRecognizer.locationInView(self.map)
        let newCoord : CLLocationCoordinate2D = map.convertPoint(touch, toCoordinateFromView: self.map)
        let anot = MKPointAnnotation()
        anot.coordinate = newCoord
        
        anot.title = "Outro lugar"
        anot.subtitle = "ok"
        
        map.addAnnotation(anot)
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLoc = locations[0] //as! CLLocation
        
        let lat = userLoc.coordinate.latitude
        let long = userLoc.coordinate.longitude
        
        let latDel : CLLocationDegrees = 0.01
        let longDel : CLLocationDegrees = 0.01
        
        let span : MKCoordinateSpan = MKCoordinateSpanMake(latDel, longDel)
        let local : CLLocationCoordinate2D = CLLocationCoordinate2DMake(lat, long)
        
        let region : MKCoordinateRegion = MKCoordinateRegionMake(local, span)
        map.setRegion(region, animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

