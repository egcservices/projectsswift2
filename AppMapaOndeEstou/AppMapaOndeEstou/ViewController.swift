//
//  ViewController.swift
//  AppMapaOndeEstou
//
//  Created by Bruno Rocha on 02/08/16.
//  Copyright © 2016 Bruno Rocha. All rights reserved.
//

import UIKit
import CoreLocation

class ViewController: UIViewController, CLLocationManagerDelegate {

    
    @IBOutlet weak var lblLatitude: UILabel!
    @IBOutlet weak var lblLongitude: UILabel!
    @IBOutlet weak var lblDirecao: UILabel!
    @IBOutlet weak var lblAltitude: UILabel!
    @IBOutlet weak var lblVeloc: UILabel!
    @IBOutlet weak var lblEndereco: UILabel!
    
    var manager : CLLocationManager!    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        manager = CLLocationManager()
        manager.delegate = self
        manager.desiredAccuracy = kCLLocationAccuracyBest
        manager.requestWhenInUseAuthorization()
        manager.startUpdatingLocation()
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let myLocation : CLLocation = locations[0]
        
        lblLatitude.text = "\(myLocation.coordinate.latitude)"
        lblLongitude.text = "\(myLocation.coordinate.longitude)"
        lblDirecao.text = "\(myLocation.course)"
        lblAltitude.text = "\(myLocation.altitude)"
        lblVeloc.text = "\(myLocation.speed)"
        
        CLGeocoder().reverseGeocodeLocation(myLocation) { (placemarks, error) -> Void in
            if error != nil{
                print(error)
            }else{
                let place = placemarks?[0]
                let userPlacemark = CLPlacemark(placemark: place!)
                self.lblEndereco.text = "\(userPlacemark.subLocality!) \(userPlacemark.subAdministrativeArea!) \(userPlacemark.postalCode!) \(userPlacemark.country!)"
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

