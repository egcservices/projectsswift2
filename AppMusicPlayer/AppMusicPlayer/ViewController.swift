//
//  ViewController.swift
//  AppMusicPlayer
//
//  Created by Bruno Rocha on 05/08/16.
//  Copyright © 2016 Bruno Rocha. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: UIViewController {
    
    @IBOutlet weak var volumeSlider: UISlider!
    @IBOutlet weak var scrubSlider: UISlider!
    
    var player = AVAudioPlayer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        iniciarPlayer()
        scrubSlider.maximumValue = Float(player.duration)
        
        NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: Selector("musica"), userInfo: nil, repeats: true)
    }
    
    func musica(){
        scrubSlider.value = Float(player.currentTime)
    }
    
    func iniciarPlayer(){
        do {
            try player = AVAudioPlayer(contentsOfURL: NSURL(fileURLWithPath: NSBundle.mainBundle().pathForResource("bach", ofType: ".mp3")!))
        }catch{
            print("error")
        }
        player.volume = volumeSlider.value
    }
    
    
    @IBAction func playMusic(sender: AnyObject) {
        player.play()
    }
    
    @IBAction func adjustVolume(sender: AnyObject) {
        player.volume = volumeSlider.value
    }
    
    @IBAction func scrub(sender: AnyObject) {
        player.currentTime = NSTimeInterval(scrubSlider.value)
    }
        
    @IBAction func pause(sender: AnyObject) {
        player.pause()
    }
    
    @IBAction func stop(sender: AnyObject) {
        player.stop()
        iniciarPlayer()
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

