//
//  DetailViewController.swift
//  AppOnTheLine
//
//  Created by Bruno Rocha on 17/08/16.
//  Copyright © 2016 Bruno Rocha. All rights reserved.
//

import UIKit
import CoreData

class DetailViewController: UIViewController {

    @IBOutlet var txtDescricao: UITextField!
    @IBOutlet var imgPost: UIImageView!
    @IBOutlet var lblPostadoPor: UILabel!
    @IBOutlet var lblPostadoAs: UILabel!
    @IBOutlet var lblAltApg: UILabel!
    @IBOutlet var btnBarAlterar: UIBarButtonItem!
    @IBOutlet var btnBarSalvar: UIBarButtonItem!
    @IBOutlet var btnBarApagar: UIBarButtonItem!
    
    var myActivityIndicator = UIActivityIndicatorView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        iniciarComponentes()
    
        if postSelecionado.getIdUser() == usuario.getId(){
            self.btnBarAlterar.enabled = true
            self.btnBarSalvar.enabled = false
            self.btnBarApagar.enabled = true
        }
        
        carregarDados()
    }
    
    func iniciarComponentes(){
        myActivityIndicator = UIActivityIndicatorView(frame: CGRectMake(0,0,50,50))
        myActivityIndicator.center = self.view.center
        myActivityIndicator.hidesWhenStopped = true
        myActivityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.Gray
        self.view.addSubview(myActivityIndicator)
    }

    func carregarDados(){
        self.activityInSwitch(true)
        lblAltApg.hidden = false
        lblAltApg.text = "Carregando..."
        
        txtDescricao.text = postSelecionado.getTexto()
        lblPostadoPor.text = "Postado por: \(postSelecionado.getNameUser())"
        lblPostadoAs.text = "Postado em: \(postSelecionado.getDataHora())"
        
        //carrega a imagem
        let url = NSURL(string: "http://egcservices.com.br/webservices/ios/on_the_line/imagens/\(postSelecionado.getImage())")!
        let task = NSURLSession.sharedSession().dataTaskWithURL(url) { data, response, error in
            
            if(error != nil){
                print("error = \(error!)")
                self.activityInSwitch(false)
                self.lblAltApg.hidden = true
                self.exibirAlert("Algo parece estranho!", msg:"Houve um erro ao baixar a imagem, por favor tente novamente!", nameBtn:"Continuar")
                return
            }
            
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.imgPost.image = UIImage(data: data!)
                self.activityInSwitch(false)
                self.lblAltApg.hidden = true
            })
        }
        task.resume()
    }
    
    @IBAction func alterarPost(sender: AnyObject) {
        txtDescricao.enabled = true
        btnBarAlterar.enabled = false
        btnBarSalvar.enabled = true
        btnBarApagar.enabled = false
    }
    
    @IBAction func salvarAltPost(sender: AnyObject) {
        activityInSwitch(true)
        
        if txtDescricao.text != "" {
            
            lblAltApg.hidden = false
            lblAltApg.text = "Atualizando..."
            
            let urlWBS = NSURL(string:"http://egcservices.com.br/webservices/ios/on_the_line/atualizar_post.php")!
            let request = NSMutableURLRequest(URL:urlWBS);
            request.HTTPMethod = "POST"
            
            let body = NSMutableData()
            let boundary = "Boundary-\(NSUUID().UUIDString)"
            
            request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
            
            body.appendString("--\(boundary)\r\n")
            body.appendString("Content-Disposition: form-data; name=\"idPost\"\r\n\r\n")
            body.appendString("\(postSelecionado.getId())\r\n")
            body.appendString("--\(boundary)\r\n")
            
            body.appendString("Content-Disposition: form-data; name=\"post\"\r\n\r\n")
            body.appendString("\(txtDescricao.text!)\r\n")
            body.appendString("--\(boundary)--\r\n")
            
            request.HTTPBody = body as NSData
            
            let task = NSURLSession.sharedSession().dataTaskWithRequest(request) { data, response, error in
                
                if(error != nil){
                    print("error = \(error!)")
                    self.lblAltApg.hidden = true
                    self.exibirAlert("Aconteceu um erro!", msg:"Por favor, tente novamente!", nameBtn:"Continuar")
                    return
                }
                
                do{
                    let myJSON = try NSJSONSerialization.JSONObjectWithData(data!, options: .MutableContainers) as? NSDictionary
                    
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                        
                        let result = myJSON!["success"] as! String
                        if (result == "1"){
                            
                            self.atualizarComps()
                            self.exibirAlert("Post Atualizado", msg:"Seu post foi atualizado com sucesso!", nameBtn:"OK")
                            
                        }else{
                            
                            self.atualizarComps()
                            self.exibirAlert("Aconteceu um Erro", msg:"\(myJSON!["message"])", nameBtn:"Continuar")
                        }
                    })
                    
                }catch{
                    
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                        self.atualizarComps()
                        self.exibirAlert("Aconteceu um erro!", msg:"Por favor, tente novamente!", nameBtn:"Continuar")
                    })
                }
            }
            task.resume()
            
        }else{
            self.activityInSwitch(false)
            self.lblAltApg.hidden = true
            self.exibirAlert("Verificar Campos", msg:"Verifique os campos!", nameBtn:"Continuar")
        }
        
    }
    
    func atualizarComps(){
        self.activityInSwitch(false)
        self.lblAltApg.hidden = true
        self.txtDescricao.enabled = false
        self.btnBarAlterar.enabled = true
        self.btnBarSalvar.enabled = false
        self.btnBarApagar.enabled = true
    }
    
    @IBAction func apagarPost(sender: AnyObject) {
        let alert = UIAlertController(title: "Apagar", message: "Deseja realmente apagar este post?", preferredStyle:UIAlertControllerStyle.Alert)
        
        alert.addAction(UIAlertAction(title: "Cancelar", style: UIAlertActionStyle.Cancel, handler: nil))
        
        alert.addAction(UIAlertAction(title: "Sim", style: UIAlertActionStyle.Default, handler: { (action) -> Void in
            
            self.activityInSwitch(true)
            self.lblAltApg.hidden = false
            self.lblAltApg.text = "Apagando..."
            
            let urlWBS = NSURL(string:"http://egcservices.com.br/webservices/ios/on_the_line/delete_post.php")!
            let request = NSMutableURLRequest(URL:urlWBS);
            request.HTTPMethod = "POST"
            
            let body = NSMutableData()
            let boundary = "Boundary-\(NSUUID().UUIDString)"
            
            request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
            
            body.appendString("--\(boundary)\r\n")
            body.appendString("Content-Disposition: form-data; name=\"idPost\"\r\n\r\n")
            body.appendString("\(postSelecionado.getId())\r\n")
            body.appendString("--\(boundary)\r\n")

            request.HTTPBody = body as NSData
            
            let task = NSURLSession.sharedSession().dataTaskWithRequest(request) { data, response, error in
                
                if(error != nil){
                    print("error = \(error!)")
                    self.lblAltApg.hidden = true
                    self.exibirAlert("Aconteceu um erro!", msg:"Por favor, tente novamente!", nameBtn:"Continuar")
                    return
                }
                
                do{
                    let myJSON = try NSJSONSerialization.JSONObjectWithData(data!, options: .MutableContainers) as? NSDictionary
                    
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                        
                        let result = myJSON!["success"] as! String
                        if (result == "1"){
                            
                            self.activityInSwitch(false)
                            self.txtDescricao.enabled = false
                            self.btnBarAlterar.enabled = false
                            self.btnBarSalvar.enabled = false
                            self.btnBarApagar.enabled = false
                            self.lblAltApg.hidden = true
                            self.exibirAlert("Post Apagado", msg:"Seu post foi apagado com sucesso!", nameBtn:"OK")
                            
                        }else{
                            
                            self.activityInSwitch(false)
                            self.lblAltApg.hidden = true
                            self.exibirAlert("Aconteceu um Erro", msg:"\(myJSON!["message"])", nameBtn:"Continuar")
                        }
                    })
                    
                }catch{
                    
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                        self.activityInSwitch(false)
                        self.lblAltApg.hidden = true
                        self.exibirAlert("Aconteceu um erro!", msg:"Por favor, tente novamente!", nameBtn:"Continuar")
                    })
                }
            }
            task.resume()
        }))
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    func activityInSwitch(ligar:Bool){
        if (ligar){
            self.myActivityIndicator.startAnimating()
            UIApplication.sharedApplication().beginIgnoringInteractionEvents()
        }else{
            self.myActivityIndicator.stopAnimating()
            UIApplication.sharedApplication().endIgnoringInteractionEvents()
        }
    }
    
    func exibirAlert(titleAlert: String, msg: String, nameBtn:String){
        let alert = UIAlertController(title: titleAlert, message: msg, preferredStyle:UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: nameBtn, style: UIAlertActionStyle.Default, handler: nil))
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
