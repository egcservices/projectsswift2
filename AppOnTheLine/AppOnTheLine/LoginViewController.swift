//
//  ViewController.swift
//  AppOnTheLine
//
//  Created by Bruno Rocha on 11/08/16.
//  Copyright © 2016 Bruno Rocha. All rights reserved.
//

import UIKit
import CoreData

var usuarios = [UsuarioLogged]()
var usuario = UsuarioClass()
var deslogou = false

class LoginViewController: UIViewController {
    
    @IBOutlet var lblNomeInfo: UILabel!
    @IBOutlet var txtNome: UITextField!
    @IBOutlet var txtEmail: UITextField!
    @IBOutlet var txtSenha: UITextField!
    @IBOutlet var btnEntrar: UIButton!
    @IBOutlet var btnCadastrar: UIButton!
    @IBOutlet var lblErro: UILabel!
    @IBOutlet var lblInfoCliente: UILabel!
   
    var managedContext: NSManagedObjectContext!
    var usuarioServices: UsuarioServices!
    var myActivityIndicator = UIActivityIndicatorView()
    
    var isCadastro = false
    var nome = "", email = "", senha = ""
    
    func iniciarComponentes(){
        myActivityIndicator = UIActivityIndicatorView(frame: CGRectMake(0,0,50,50))
        myActivityIndicator.center = self.view.center
        myActivityIndicator.hidesWhenStopped = true
        myActivityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.Gray
        self.view.addSubview(myActivityIndicator)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        iniciarComponentes()
    }
    
    override func viewDidAppear(animated: Bool) {
        activityInSwitch(true)
        managedContext = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
        usuarioServices = UsuarioServices(context: managedContext)
        usuarios = usuarioServices.listar()
        
        if usuarios.count > 0 && deslogou == false {
            usuario = UsuarioClass()
            for us in usuarios{
                usuario.setId(us.valueForKey("id") as! Int)
                usuario.setEmail(us.valueForKey("email") as! String)
                usuario.setNome(us.valueForKey("nome") as! String)
                usuario.setData(us.valueForKey("data") as! String)
            }
            self.performSegueWithIdentifier("login", sender: self)
        }
        activityInSwitch(false)
    }
    
    func activityInSwitch(ligar:Bool){
        if (ligar){
            self.myActivityIndicator.startAnimating()
            UIApplication.sharedApplication().beginIgnoringInteractionEvents()
        }else{
            self.myActivityIndicator.stopAnimating()
            UIApplication.sharedApplication().endIgnoringInteractionEvents()
        }
    }
    
    @IBAction func entrar(sender: AnyObject) {
        activityInSwitch(true)
        nome = txtNome.text!
        email = txtEmail.text!
        senha = txtSenha.text!
        
        if isCadastro{
            
            if ValidarCampos(1) && ValidarEmail(email){
                
                let urlWBS = NSURL(string:"http://egcservices.com.br/webservices/ios/on_the_line/val_cad_user.php?nome=\(FormatValue(nome))&email=\(FormatValue(email))&senha=\(FormatValue(senha))")!
                let request = NSMutableURLRequest(URL:urlWBS);
                
                let task = NSURLSession.sharedSession().dataTaskWithRequest(request) { data, response, error in
                    
                    if(error == nil){
                        
                        do{
                            let myJSON =  try NSJSONSerialization.JSONObjectWithData(data!, options: .MutableContainers) as? NSDictionary
                            
                            if let parseJSON = myJSON {
                                
                                let result = parseJSON["success"] as! Int
                                
                                if result == 1 {
                                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                                        self.nome = ""; self.email = ""; self.senha = "";
                                        self.txtNome.text = ""
                                        self.txtEmail.text = ""
                                        self.txtSenha.text = ""

                                        self.activityInSwitch(false)
                                        self.exibirAlert("Sucesso!", msg:"Seu cadastro foi efetuado com sucesso! Agora você pode realizar o login", nameBtn:"Ok")
                                        
                                        self.lblInfoCliente.text  = "Já possui uma conta?"
                                        self.btnEntrar.setTitle("Cadastrar", forState: UIControlState.Normal)
                                        self.btnCadastrar.setTitle("Entre", forState: UIControlState.Normal)
                                        self.isCadastro = false
                                    })
                                }else{
                                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                                        let message = parseJSON["message"] as? String
                                        self.activityInSwitch(false)
                                        self.exibirAlert("Ocorreu um erro!", msg: message!, nameBtn:"OK")
                                    })
                                }
                            }
                        }catch{}
                    }
                }
                task.resume()
            }else{
                self.activityInSwitch(false)
                exibirAlert("Verificar Campos", msg:"Verifique os campos!", nameBtn:"Continuar")
            }
        }else{
            if ValidarCampos(0) && ValidarEmail(email){
                
                let urlWBS = NSURL(string:"http://egcservices.com.br/webservices/ios/on_the_line/login.php?email=\(FormatValue(email))&senha=\(FormatValue(senha))")!
                let request = NSMutableURLRequest(URL:urlWBS);
                
                let task = NSURLSession.sharedSession().dataTaskWithRequest(request) { data, response, error in
                    
                    if(error == nil){
                        
                        do{
                            let myJSON =  try NSJSONSerialization.JSONObjectWithData(data!, options: .MutableContainers) as? NSDictionary
                            
                            if let parseJSON = myJSON {
                                
                                let result = parseJSON["success"] as! String
                                if (result == "1"){
                                    usuario = UsuarioClass()
                                    usuario.setId(Int(parseJSON["id"] as! String)!)
                                    usuario.setEmail(self.email)
                                    usuario.setSenha(self.senha)
                                    usuario.setNome(parseJSON["nome"] as! String)
                                    usuario.setData(parseJSON["data"] as! String)
                                    
                                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                                        self.usuarioServices.cadastrar(usuario)
                                        if self.saveChanges() == "1"{
                                            self.txtEmail.text = ""
                                            self.txtSenha.text = ""
                                            
                                            self.activityInSwitch(false)
                                            //entra na tela 2
                                            self.performSegueWithIdentifier("login", sender: self)
                                        }
                                    })
                                    
                                }else{
                                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                                        self.activityInSwitch(false)
                                        let message = parseJSON["message"] as? String
                                        self.exibirAlert("Dados Incorretos!", msg: message!, nameBtn:"OK")
                                    })
                                }
                            }else{
                                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                                    self.activityInSwitch(false)
                                    self.exibirAlert("Dados Incorretos!", msg: "Dados não estão corretos!", nameBtn:"OK")
                                })
                            }
                        }catch{}
                    }
                }
                task.resume()
            }else{
                self.activityInSwitch(false)
                exibirAlert("Verificar Campos", msg:"Verifique os campos!", nameBtn:"Continuar")
            }
        }
    }
    
    func exibirAlert(titleAlert: String, msg: String, nameBtn:String){
        let alert = UIAlertController(title: titleAlert, message: msg, preferredStyle:UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: nameBtn, style: UIAlertActionStyle.Default, handler: { (action) -> Void in
            self.dismissViewControllerAnimated(true, completion:nil)
        }))
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    func ValidarCampos(tipo: Int) -> Bool{
        switch(tipo){
            case 0:
                if (txtEmail.text == "" || txtSenha.text == "" ){
                    return false
                }
                break
            case 1:
                if (txtNome.text == "" || txtEmail.text == "" || txtSenha.text == "" ){
                    return false
                }
                break
            default:
                return false
        }
        return true
    }
    
    func ValidarEmail(enteredEmail: String)->Bool{
        let emailFormat = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailFormat)
        return emailPredicate.evaluateWithObject(enteredEmail)
    }
    
    func FormatValue(value: String) -> String{
        let valueFormat = value.stringByReplacingOccurrencesOfString(" ", withString: "+", options: NSStringCompareOptions.CaseInsensitiveSearch, range: nil)
        return valueFormat.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!
    }

    @IBAction func cadastrar(sender: AnyObject) {
        var value = false
        
        if isCadastro{
            value = true
            lblInfoCliente.text  = "Não possui uma conta?"
            btnEntrar.setTitle("Entrar", forState: UIControlState.Normal)
            btnCadastrar.setTitle("Cadastre-se", forState: UIControlState.Normal)
            isCadastro = false
        }else{
            lblInfoCliente.text  = "Já possui uma conta?"
            btnEntrar.setTitle("Cadastrar", forState: UIControlState.Normal)
            btnCadastrar.setTitle("Entre", forState: UIControlState.Normal)
            isCadastro = true
        }
        lblNomeInfo.hidden = value
        txtNome.hidden = value
    }

    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
    }

    func saveChanges() -> String{
        do{
            try self.managedContext.save()
            return "1"
        }catch{
            return "0"
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

