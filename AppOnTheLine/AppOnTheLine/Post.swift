//
//  Post.swift
//  AppOnTheLine
//
//  Created by Bruno Rocha on 17/08/16.
//  Copyright © 2016 Bruno Rocha. All rights reserved.
//

import Foundation
import CoreData

extension Post {
    @NSManaged var id: Int32
    @NSManaged var id_user: Int32
    @NSManaged var text_post: String?
    @NSManaged var data_hora_post: String?
    @NSManaged var image: String?
}

class Post: NSManagedObject {
    static let entityName = "Post"
}