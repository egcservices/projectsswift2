//
//  PostClass.swift
//  AppOnTheLine
//
//  Created by Bruno Rocha on 17/08/16.
//  Copyright © 2016 Bruno Rocha. All rights reserved.
//

import Foundation

class PostClass{
    private var id: Int!
    private var idUser : Int!
    private var nameUser: String!
    private var texto: String!
    private var dataHora: String!
    private var image: String!
    
    func getId() -> Int{
        return self.id
    }
    
    func getIdUser() ->Int{
        return self.idUser
    }
    
    func getNameUser() ->String{
        return self.nameUser
    }
    
    func getTexto() ->String{
        return self.texto
    }
    
    func getDataHora()->String{
        return self.dataHora
    }
    
    func getImage()->String{
        return self.image
    }
    
    func setId(id:Int){
        self.id = id
    }
    
    func setIdUser(idUser:Int){
        self.idUser = idUser
    }
    
    func setNameUser(nameUser:String){
        self.nameUser = nameUser
    }
    
    func setTexto(texto:String){
        self.texto = texto
    }
    
    func setDataHora(dtH:String){
        self.dataHora = dtH
    }
    
    func setImage(img:String){
        self.image = img
    }
}