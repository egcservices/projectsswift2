//
//  PostarViewController.swift
//  AppOnTheLine
//
//  Created by Bruno Rocha on 17/08/16.
//  Copyright © 2016 Bruno Rocha. All rights reserved.
//

import UIKit

extension NSMutableData {
    
    func appendString(string: String) {
        let data = string.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: true)
        appendData(data!)
    }
}

class PostarViewController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {

    @IBOutlet var btnSelecionar: UIButton!
    @IBOutlet var imgPost: UIImageView!
    @IBOutlet var txtDescricao: UITextField!
    @IBOutlet var lblEnviando: UILabel!
    
    var myActivityIndicator = UIActivityIndicatorView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        iniciarComponentes()
    }
    
    func iniciarComponentes(){
        myActivityIndicator = UIActivityIndicatorView(frame: CGRectMake(0,0,50,50))
        myActivityIndicator.center = self.view.center
        myActivityIndicator.hidesWhenStopped = true
        myActivityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.Gray
        self.view.addSubview(myActivityIndicator)
    }
    
    @IBAction func selecionarImg(sender: AnyObject) {
        selectType(0)
    }
    
    @IBAction func tirarFoto(sender: AnyObject) {
        selectType(1)
    }
    
    func selectType(tipo:Int){
        let image = UIImagePickerController()
        image.delegate = self
        if tipo == 0 {
            image.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
        }else{
            image.sourceType = UIImagePickerControllerSourceType.Camera
        }
        image.allowsEditing = false
        self.presentViewController(image, animated: true, completion: nil)
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        imgPost.image = info[UIImagePickerControllerOriginalImage] as? UIImage
        
        self.dismissViewControllerAnimated(true, completion: nil)
    }

    func createBodyWithParameters(parameters: [String: String]?, filePathKey: String?, imageDataKey: NSData, boundary: String) -> NSData {
        let body = NSMutableData()
        
        if parameters != nil {
            for (key, value) in parameters! {
                body.appendString("--\(boundary)\r\n")
                body.appendString("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                body.appendString("\(value)\r\n")
            }
        }
        
        let filename = "myImage.jpg"
        let mimetype = "image/jpg"
        
        body.appendString("--\(boundary)\r\n")
        body.appendString("Content-Disposition: form-data; name=\"\(filePathKey!)\"; filename=\"\(filename)\"\r\n")
        body.appendString("Content-Type: \(mimetype)\r\n\r\n")
        body.appendData(imageDataKey)
        body.appendString("\r\n")
        
        body.appendString("--\(boundary)--\r\n")
        
        return body
    }

    @IBAction func enviarPost(sender: AnyObject) {
        activityInSwitch(true)
        
        if imgPost.image != nil && txtDescricao.text != "" {
            
            lblEnviando.hidden = false
            let urlWBS = NSURL(string:"http://egcservices.com.br/webservices/ios/on_the_line/salvar_post.php")!
            let request = NSMutableURLRequest(URL:urlWBS);
            request.HTTPMethod = "POST"
            
            let param = [
                "user"  : "\(usuario.getId())",
                "post"    : "\(txtDescricao.text!)"
            ]
            
            let boundary = "Boundary-\(NSUUID().UUIDString)"
            
            request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
            
            let imageData = UIImageJPEGRepresentation(imgPost.image!, 1)
            if imageData == nil { return }
            
            request.HTTPBody = createBodyWithParameters(param, filePathKey: "file", imageDataKey: imageData!, boundary: boundary)
            
            let task = NSURLSession.sharedSession().dataTaskWithRequest(request) { data, response, error in
           
                if(error != nil){
                    print("error = \(error!)")
                    self.lblEnviando.hidden = true
                    self.exibirAlert("Aconteceu um erro!", msg:"Por favor, tente novamente!", nameBtn:"Continuar")
                    return
                }
                
                do{
                    let myJSON = try NSJSONSerialization.JSONObjectWithData(data!, options: .MutableContainers) as? NSDictionary
                    
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                        
                        let result = myJSON!["success"] as! String
                        if (result == "1"){

                            self.activityInSwitch(false)
                            self.txtDescricao.text = ""
                            self.imgPost.image = UIImage(named: "placeholder.png")
                            self.lblEnviando.hidden = true
                            self.exibirAlert("Post Enviado", msg:"Seu post foi enviado com sucesso!", nameBtn:"OK")
                            
                        }else{
                            
                            self.activityInSwitch(false)
                            self.lblEnviando.hidden = true
                            self.exibirAlert("Aconteceu um Erro", msg:"\(myJSON!["message"])", nameBtn:"Continuar")
                        }
                    })
                    
                }catch{
                    
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                        self.activityInSwitch(false)
                        self.lblEnviando.hidden = true
                        self.exibirAlert("Aconteceu um erro!", msg:"Por favor, tente novamente!", nameBtn:"Continuar")
                    })
                }
            }
            task.resume()
            
        }else{
            self.activityInSwitch(false)
            self.lblEnviando.hidden = true
            self.exibirAlert("Verificar Campos", msg:"Verifique os campos!", nameBtn:"Continuar")
        }
    }
    
    func activityInSwitch(ligar:Bool){
        if (ligar){
            self.myActivityIndicator.startAnimating()
            UIApplication.sharedApplication().beginIgnoringInteractionEvents()
        }else{
            self.myActivityIndicator.stopAnimating()
            UIApplication.sharedApplication().endIgnoringInteractionEvents()
        }
    }
    
    func exibirAlert(titleAlert: String, msg: String, nameBtn:String){
        let alert = UIAlertController(title: titleAlert, message: msg, preferredStyle:UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: nameBtn, style: UIAlertActionStyle.Default, handler: nil))
        self.presentViewController(alert, animated: true, completion: nil)
    }

    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}