//
//  TableViewController.swift
//  AppOnTheLine
//
//  Created by Bruno Rocha on 17/08/16.
//  Copyright © 2016 Bruno Rocha. All rights reserved.
//

import UIKit
import CoreData

var postSelecionado : PostClass!

class TableViewController: UITableViewController {
    
    var managedContext: NSManagedObjectContext!
    var posts = [PostClass]()
    //var postServices: PostServices!
    var usuarioServices: UsuarioServices!
    var myActivityIndicator = UIActivityIndicatorView()
    var refresher: UIRefreshControl!
    
    @IBAction func realizarLogOff(sender: AnyObject) {
        
        if usuarios.count > 0 {
            for user in usuarios{
                usuarioServices.remover(user.objectID)
            }
            usuarios.removeAll()
            saveChanges()
        }
        deslogou = true
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func activityInSwitch(ligar:Bool){
        if (ligar){
            self.myActivityIndicator.startAnimating()
            UIApplication.sharedApplication().beginIgnoringInteractionEvents()
        }else{
            self.myActivityIndicator.stopAnimating()
            UIApplication.sharedApplication().endIgnoringInteractionEvents()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        managedContext = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
        usuarioServices = UsuarioServices(context: managedContext)
        
        refresher = UIRefreshControl()
        refresher.attributedTitle = NSAttributedString(string: "Puxe para atualizar")
        refresher.addTarget(self, action: "refresh", forControlEvents: UIControlEvents.ValueChanged)
        self.tableView.addSubview(refresher)
        
        refresh()
    }
    
    func refresh(){
        listarPostWeb()
    }
    
    func listarPostWeb() {
        activityInSwitch(true)
        self.posts.removeAll(keepCapacity: true)
        self.tableView.reloadData()
        
        let urlWBS = NSURL(string:"http://egcservices.com.br/webservices/ios/on_the_line/listar_posts.php")!
        let request = NSMutableURLRequest(URL:urlWBS);
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request) { data, response, error in
            
            if(error == nil){
                
                do{
                    let myJSON =  try NSJSONSerialization.JSONObjectWithData(data!, options: .MutableContainers) as? NSDictionary
                    print(myJSON)
                    
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    
                        if let parseJSON = myJSON {
                                
                            let results = parseJSON["posts"] as! NSArray
                            
                            if (results.count > 0){
                                for result in results{
                                    let post = PostClass()
                                    post.setId(Int(result["id"] as! String)!)
                                    post.setIdUser(Int(result["user"] as! String)!)
                                    post.setNameUser(result["nameUser"] as! String)
                                    post.setImage(result["image"] as! String)
                                    post.setTexto(result["text"] as! String)
                                    post.setDataHora(result["data"] as! String)
                                    self.posts.append(post)
                                }
                            }else{
                                self.exibirAlert("Sem Posts!", msg: "Sem posts no momento!", nameBtn:"OK")
                            }
                            
                            self.tableView.reloadData()
                            self.refresher.endRefreshing()
                            self.activityInSwitch(false)
                            
                        }else{
                            
                            self.tableView.reloadData()
                            self.refresher.endRefreshing()
                            self.activityInSwitch(false)
                            self.exibirAlert("Sem Posts!", msg: "Sem posts no momento!", nameBtn:"OK")
                        }
                    })
                }catch{}
            }
        }
        task.resume()
    }
    
    func exibirAlert(titleAlert: String, msg: String, nameBtn:String){
        let alert = UIAlertController(title: titleAlert, message: msg, preferredStyle:UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: nameBtn, style: UIAlertActionStyle.Default, handler: { (action) -> Void in
            self.dismissViewControllerAnimated(true, completion:nil)
        }))
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return posts.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("myCell", forIndexPath: indexPath)
        let mPost = posts[indexPath.row]
        
        cell.textLabel?.text = "\(mPost.getTexto())" // - \(mPost.getDataHora())"
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        postSelecionado = posts[indexPath.row]
        self.refresher.endRefreshing()
    }
    
    func saveChanges() -> String{
        do{
            try self.managedContext.save()
            return "1"
        }catch{
            return "0"
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
