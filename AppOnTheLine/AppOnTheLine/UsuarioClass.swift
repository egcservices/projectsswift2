//
//  UsuarioClass.swift
//  AppOnTheLine
//
//  Created by Bruno Rocha on 16/08/16.
//  Copyright © 2016 Bruno Rocha. All rights reserved.
//

import Foundation

class UsuarioClass{
    private var id: Int!
    private var email : String!
    private var senha: String!
    private var nome: String!
    private var data: String!
    
    func getId() -> Int{
        return self.id
    }
    
    func getEmail() ->String{
        return self.email
    }
    
    func getSenha() ->String{
        return self.senha
    }
    
    func getNome()->String{
        return self.nome
    }
    
    func getData()->String{
        return self.data
    }
    
    func setId(id:Int){
        self.id = id
    }
    
    func setEmail(email:String){
        self.email = email
    }
    
    func setSenha(senha:String){
        self.senha = senha
    }
    
    func setNome(nome:String){
        self.nome = nome
    }
    
    func setData(data:String){
        self.data = data
    }
    
}