//
//  Usuario.swift
//  AppOnTheLine
//
//  Created by Bruno Rocha on 16/08/16.
//  Copyright © 2016 Bruno Rocha. All rights reserved.
//

import Foundation
import CoreData

extension UsuarioLogged {
    @NSManaged var id: Int32
    @NSManaged var email: String?
    @NSManaged var senha: String?
    @NSManaged var nome: String?
    @NSManaged var data: String?
}

class UsuarioLogged: NSManagedObject {
    static let entityName = "UsuarioLogged"
}