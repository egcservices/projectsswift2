//
//  UsuarioServices.swift
//  AppOnTheLine
//
//  Created by Bruno Rocha on 16/08/16.
//  Copyright © 2016 Bruno Rocha. All rights reserved.
//

import Foundation
import CoreData

class UsuarioServices{
    
    var context: NSManagedObjectContext
    
    init(context: NSManagedObjectContext){
        self.context = context
    }
    
    func cadastrar(userSalvar:UsuarioClass) -> UsuarioLogged {
        let usuario = NSEntityDescription.insertNewObjectForEntityForName(UsuarioLogged.entityName, inManagedObjectContext: context) as! UsuarioLogged
        
        usuario.id = Int32(userSalvar.getId())
        usuario.email = userSalvar.getEmail()
        usuario.senha = userSalvar.getSenha()
        usuario.nome = userSalvar.getNome()
        usuario.data = userSalvar.getData()
        
        return usuario
    }
    
    func listar() -> [UsuarioLogged]{
        let myFetchRequest = NSFetchRequest(entityName: UsuarioLogged.entityName)
        do {
            let response = try context.executeFetchRequest(myFetchRequest)
            return response as! [UsuarioLogged]
        } catch let error as NSError {
            // failure
            print(error)
            return [UsuarioLogged]()
        }
    }
    
    func remover(id: NSManagedObjectID){
        context.deleteObject((context.objectWithID(id) as? UsuarioLogged)!)
    }
}