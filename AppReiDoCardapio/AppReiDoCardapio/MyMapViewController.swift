//
//  MyMapViewController.swift
//  AppReiDoCardapio
//
//  Created by Bruno Rocha on 13/09/16.
//  Copyright © 2016 Bruno Rocha. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces

class MyMapViewController: UIViewController {

    //var camera : GMSCameraPosition!
    var tmapView : GMSMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let lat = CLLocationDegrees(-8.116649)
        let lon = CLLocationDegrees(-35.292928)
        
        let cam = GMSCameraPosition.cameraWithLatitude(lat, longitude: lon, zoom: 16.0)
        atualizarMapa(cam)
    }
    
    func atualizarMapa(cam : GMSCameraPosition) -> Void {
        
        tmapView = GMSMapView.mapWithFrame(CGRect.zero, camera: cam)
        tmapView.myLocationEnabled = true
//        tmapView.myLocation?.coordinate.latitude = cam.va
        tmapView.settings.compassButton = true
        tmapView.settings.myLocationButton = true
        tmapView.settings.zoomGestures = true
        self.view = tmapView
        print("my loc: \(tmapView.myLocation?.coordinate.latitude) e \(tmapView.myLocation?.coordinate.longitude)")
    
    }
    
     func alterar(txt : String) -> Void {
        
        let cam = GMSCameraPosition.cameraWithLatitude(143.116649, longitude: 155.292928, zoom: 16.0)
        atualizarMapa(cam)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
