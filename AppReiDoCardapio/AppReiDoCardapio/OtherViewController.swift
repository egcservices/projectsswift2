//
//  OtherViewController.swift
//  AppReiDoCardapio
//
//  Created by Bruno Rocha on 13/09/16.
//  Copyright © 2016 Bruno Rocha. All rights reserved.
//

import UIKit
import CoreLocation

class OtherViewController: UIViewController {
    
    @IBOutlet var txtKms: UITextField!
    @IBOutlet var myContainer: UIView!
    
    //let myMapViewController : MyMapViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    @IBAction func procurar(sender: AnyObject) {
        let myMapViewController = MyMapViewController()
        print(txtKms.text!)
        myMapViewController.alterar(txtKms.text!)
        
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}