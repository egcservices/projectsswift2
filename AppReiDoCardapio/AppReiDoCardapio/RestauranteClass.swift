//
//  Restaurante.swift
//  AppReiDoCardapio
//
//  Created by Bruno Rocha on 01/09/16.
//  Copyright © 2016 Bruno Rocha. All rights reserved.
//

import Foundation

class RestauranteClass{
    
    private var id: Int!
    private var nome : String!
    private var endereco: String!
    private var latitude: String!
    private var longitude: String!
    private var kms: String!
    
    func getId() -> Int{
        return self.id
    }
    
    func getNome() -> String{
        return self.nome
    }
    
    func getEndereco() -> String{
        return self.endereco
    }
    
    func getLatitude() -> String{
        return self.latitude
    }
    
    func getLongitude() -> String{
        return self.longitude
    }
    
    func getKms() -> String{
        return self.kms
    }
    
    func setRestaurante(id: Int, nome:String, endereco:String, lat:String, long:String, kms:String){
        self.id = id
        self.nome = nome
        self.endereco = endereco
        self.latitude = lat
        self.longitude = long
        self.kms = kms
    }
}