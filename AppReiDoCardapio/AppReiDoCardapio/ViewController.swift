//
//  ViewController.swift
//  AppReiDoCardapio
//
//  Created by Bruno Rocha on 31/08/16.
//  Copyright © 2016 Bruno Rocha. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import GoogleMaps

extension NSMutableData {
    
    func appendString(string: String) {
        let data = string.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: true)
        appendData(data!)
    }
}

class ViewController: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate {

    @IBOutlet var txtKms: UITextField!
    @IBOutlet var mapView: MKMapView!
    @IBOutlet var barBtnParar : UIBarButtonItem!
    
    var qtdRota : Int = 0
    var myRoute : MKRoute!
    var destination : MKPointAnnotation!
    
    var locManager : CLLocationManager!
    var userLocation : CLLocation!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        locManager = CLLocationManager()
        locManager.delegate = self
        locManager.desiredAccuracy = kCLLocationAccuracyBest
        locManager.requestWhenInUseAuthorization()
        //locManager.startUpdatingLocation()
        
        //Setup our Map View
//        self.mapView.delegate = self
//        //mapView.mapType = MKMapType.Satellite
//        self.mapView.showsUserLocation = true
//        self.mapView.userLocation.title = "Minha Localização"
        
        //praça da matriz
        //-8.116649, -35.292928
        
        let camera = GMSCameraPosition.cameraWithLatitude(-8.116649, longitude: -35.292928, zoom: 16.0)
        let tmapView = GMSMapView.mapWithFrame(CGRect.zero, camera: camera)
        tmapView.myLocationEnabled = true
        tmapView.settings.compassButton = true
        tmapView.settings.myLocationButton = true
        tmapView.settings.zoomGestures = true
        view = tmapView
        
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: -8.116649, longitude: -35.292928)
        marker.title = "Sydney"
        marker.snippet = "Australia"
        marker.map = tmapView
        
        if (UIApplication.sharedApplication().canOpenURL(NSURL(string:"comgooglemaps://")!)) {
        
            print("pode usar")
            
        } else {
            print("Can't use comgooglemaps://");
        }
        
    }
        
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        //userLocation = locations[0]
//        let camera = GMSCameraPosition.cameraWithLatitude(-8.116649, longitude: -35.292928, zoom: 6.0)
//        let tmapView = GMSMapView.mapWithFrame(CGRect.zero, camera: camera)
//        tmapView.myLocationEnabled = true
//        view = tmapView
        
        // Creates a marker in the center of the map.
//        let marker = GMSMarker()
//        marker.position = CLLocationCoordinate2D(latitude: -33.86, longitude: 151.20)
//        marker.title = "Sydney"
//        marker.snippet = "Australia"
//        marker.map = tmapView
        
//        userLocation = locations[0]
//        let coordinate = CLLocationCoordinate2DMake(userLocation.coordinate.latitude, userLocation.coordinate.longitude)
//        let latDelta : CLLocationDegrees = 0.005
//        let lonDelta : CLLocationDegrees = 0.005
//        let span : MKCoordinateSpan = MKCoordinateSpanMake(latDelta, lonDelta)
//        let region : MKCoordinateRegion = MKCoordinateRegionMake(coordinate, span)
//        self.mapView.setRegion(region, animated: true)
    }
    
    @IBAction func procurarRestaurantes(sender: AnyObject) {
        
        if txtKms.text != ""{
            
            let myKm = Float(txtKms.text!)!
            
            let urlWBS = NSURL(string:"http://egcservices.com.br/webservices/ios/cardapio/pesquisar_restaurante.php")!
            let request = NSMutableURLRequest(URL:urlWBS)
            request.HTTPMethod = "POST"
            
            let body = NSMutableData()
            let boundary = "Boundary-\(NSUUID().UUIDString)"
            
            request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
            
            body.appendString("--\(boundary)\r\n")
            body.appendString("Content-Disposition: form-data; name=\"lat\"\r\n\r\n")
            body.appendString("\(userLocation.coordinate.latitude)\r\n")
            
            body.appendString("--\(boundary)\r\n")
            body.appendString("Content-Disposition: form-data; name=\"long\"\r\n\r\n")
            body.appendString("\(userLocation.coordinate.longitude)\r\n")
            
            body.appendString("--\(boundary)\r\n")
            body.appendString("Content-Disposition: form-data; name=\"raio\"\r\n\r\n")
            body.appendString("\(myKm)\r\n")
            
            body.appendString("--\(boundary)\r\n")

            request.HTTPBody = body as NSData
            
            let task = NSURLSession.sharedSession().dataTaskWithRequest(request) { data, response, error in
                
                if(error != nil){
                    print("error = \(error!)")
                    
                    let alert = UIAlertController(title: "Aconteceu um Erro!", message: "Por favor, tente novamente!", preferredStyle:UIAlertControllerStyle.Alert)
                    alert.addAction(UIAlertAction(title: "Continuar", style: UIAlertActionStyle.Default, handler: nil))
                    self.presentViewController(alert, animated: true, completion: nil)
                    
                    return
                }
                
                do{
                    let myJSON = try NSJSONSerialization.JSONObjectWithData(data!, options: .MutableContainers) as? NSDictionary
                    
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                        
                        if let parseJSON = myJSON {
                            var rests = [RestauranteClass]()
                            let results = parseJSON["restaurantes"] as! NSArray
                            
                            if (results.count > 0){
                                var title = "Locais Próximos"
                                var msg = "Foram encontrados esses locais próximos a você no raio de \(myKm) kms."
                                
                                if results.count == 1{
                                    title = "Local Próximo"
                                    msg = "Foi encontrado esse local próximo a você no raio de \(myKm) kms."
                                }
                                
                                let alert = UIAlertController(title: title, message: msg, preferredStyle:UIAlertControllerStyle.Alert)
                                
                                for result in results {
                                    let rest = RestauranteClass()
                                    
                                    let i = Int(result["id"] as! String)!
                                    let n = result["nome"] as! String
                                    let e = result["endereco"] as! String
                                    let lt = result["latitude"] as! String
                                    let ln = result["longitude"] as! String
                                    let k = result["kms"] as! String
                                    
                                    rest.setRestaurante(i, nome: n, endereco: e, lat: lt, long: ln, kms: k)
                                    
                                    rests.append(rest)
                                    
                                    alert.addAction(UIAlertAction(title: "\(rest.getNome()) - \(rest.getEndereco())", style: UIAlertActionStyle.Default, handler: { (action) -> Void in
                                        
                                        self.tracarRota(rest)
                                        
                                    }))
                                    
                                }
                                
                                alert.addAction(UIAlertAction(title: "Cancelar", style: UIAlertActionStyle.Cancel, handler: nil))
                                
                                self.presentViewController(alert, animated: true, completion: nil)
                                
                            }else{
                                print("não trouxe rests")
                            }
                            
                        }
                    })
                    
                }catch{ }
            }
            task.resume()
        }
    }
    
    func tracarRota(rest: RestauranteClass){
        if self.qtdRota > 0{
            self.limparRota()
        }
        
        self.destination = MKPointAnnotation()
        self.destination.coordinate = CLLocationCoordinate2DMake(Double(rest.getLatitude())!, Double(rest.getLongitude())!)
        self.destination.title = rest.getNome()
        self.mapView.addAnnotation(self.destination)
        self.mapView.centerCoordinate = self.destination.coordinate
        
        //Span of the map
        self.mapView.setRegion(MKCoordinateRegionMake(self.destination.coordinate, MKCoordinateSpanMake(0.01,0.01)), animated: true)
        
        let directionsRequest = MKDirectionsRequest()
        directionsRequest.source = MKMapItem.mapItemForCurrentLocation()
        
        let markDestination = MKPlacemark(coordinate: CLLocationCoordinate2DMake(self.destination.coordinate.latitude, self.destination.coordinate.longitude), addressDictionary: nil)
        
        directionsRequest.destination = MKMapItem(placemark: markDestination)
        
        directionsRequest.requestsAlternateRoutes = false
        
        directionsRequest.transportType = MKDirectionsTransportType.Automobile
        
        let directions = MKDirections(request: directionsRequest)
        
        directions.calculateDirectionsWithCompletionHandler({
            response, error in
            
            if error != nil {
                
                print("Error getting directions")
            } else {
                
                self.qtdRota++
                self.myRoute = response!.routes[0] as MKRoute
                self.mapView.addOverlay(self.myRoute.polyline, level:  MKOverlayLevel.AboveRoads)
                self.barBtnParar.enabled = true
                
                for step in self.myRoute.steps{
                    print(step.instructions)
                }
            }
        })
    }
      
   @IBAction func pararNavegacao(sender: AnyObject) {
        limparRota()
        self.barBtnParar.enabled = false
    }
    
    func limparRota(){
        self.qtdRota = 0
        self.mapView.removeAnnotation(self.destination)
        self.mapView.removeOverlay(self.myRoute.polyline)
        self.myRoute = MKRoute()
    }
    
    func mapView(mapView: MKMapView, rendererForOverlay overlay: MKOverlay) -> MKOverlayRenderer {
        
        let myLineRenderer = MKPolylineRenderer(overlay: overlay) //MKPolylineRenderer(polyline: myRoute.polyline)
        myLineRenderer.strokeColor = UIColor.blueColor()
        myLineRenderer.lineWidth = 5.0
        return myLineRenderer
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
//    usando api do google maps
//    if (UIApplication.sharedApplication().canOpenURL(NSURL(string:"comgooglemaps://")!)) {
//        UIApplication.sharedApplication().openURL(NSURL(string:
//            "comgooglemaps://?center=\(rest.getLatitude()),\(rest.getLongitude())&zoom=14&views=traffic")!)
//    } else {
//        print("Can't use comgooglemaps://");
//    }
}

