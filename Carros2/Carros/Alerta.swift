//
//  Alerta.swift
//  Carros
//
//  Created by Dauster on 12/10/15.
//  Copyright (c) 2015 Diogenes Dauster. All rights reserved.
//

import UIKit

class Alerta {
    class func alerta(msg: String, viewController: UIViewController ){
        let alert = UIAlertController(title: "Alerta",message: msg , preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
        viewController.presentViewController(alert, animated: true, completion: nil)
    }
    
    class func alerta(msg: String, viewController: UIViewController,action: ((UIAlertAction!) -> Void)! ){
        let alert = UIAlertController(title: "Alerta", message: msg, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: action))
        viewController.presentViewController(alert,animated: true,completion: nil)
    }
}