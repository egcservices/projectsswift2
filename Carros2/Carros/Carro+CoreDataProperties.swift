//
//  Carro+CoreDataProperties.swift
//  Carros
//
//  Created by Diogenes Dauster on 08/12/15.
//  Copyright © 2015 Diogenes Dauster. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Carro  {

    @NSManaged var url_info: String
    @NSManaged var url_video: String
    @NSManaged var latitude: String?
    @NSManaged var longitude: String?
    @NSManaged var nome: String?
    @NSManaged var timestamp: NSDate?
    @NSManaged var tipo: String?
    @NSManaged var url_foto: String?
    @NSManaged var desc: String?

}
