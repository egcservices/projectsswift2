//
//  Carro.swift
//  Carros
//
//  Created by Dauster on 12/10/15.
//  Copyright (c) 2015 Diogenes Dauster. All rights reserved.
//

import Foundation

class Carro{
    // id do banco de dados
    var id: Int = 0
    // tipo : classico , esportivo , luxo
    var tipo: String = ""
    // Nome do carro
    var nome:String = ""
    // Descricao do carro
    var desc:String = ""
    // url para cada foto do carro
    var url_foto:String = ""
    // url com um site com informacoes do carro
    var url_info:String = ""
    // url com o video do carro
    var url_video:String = ""
    // coordenadas da fabrica ou pais de origem do carro
    var latitude:String = ""
    var longitude:String = ""
}