//
//  CarroService.swift
//  Carros
//
//  Created by Dauster on 12/10/15.
//  Copyright (c) 2015 Diogenes Dauster. All rights reserved.
//

import Foundation

class CarroService{
    // metodo estatico de classe que retorna um array de carros
    class func getCarros() -> Array<Carro>{
        var carros : Array<Carro> = []
        for(var i = 0;i < 10 ; i++){
            let c = Carro()
            c.nome = "Ferrari "+String(i)
            c.desc = "Desc Ferrari "+String(i)
            c.url_foto = "Ferrari_FF.png"
            // adiciona o carro no array
            carros.append(c)
        }
        
        return carros
    }
    
    class func getCarrosByTipo(tipo: String,cache: Bool,callback: (carros:Array<Carro>, error:NSError!) -> Void){
        
        var db = CarroDB()
        // Busca ps carros do banco de dados
        let carros = cache ? db.getCarrosByTipo(tipo) : []
        // Se existir no banco de dados retorna
        
        if(carros.count > 0 ){
            db.close()
            // retorna os carros pela função de retorno
            callback(carros: carros, error: nil)
            print("Retornando Carris \(tipo) do banco")
            return
        }
        
        let http = NSURLSession.sharedSession()
        let url  = NSURL(string: "http://www.livroiphone.com.br/carros/carros_"+tipo+".json")!
        let task  = http.dataTaskWithURL(url, completionHandler:{
            (data:NSData?,response: NSURLResponse?,error: NSError?) -> Void in
            if (error != nil){
                callback(carros:[], error:error)
            }else{
                let carros = CarroService.parserJson(data!)
                if(carros.count > 0 ){
                    // Salva os carros no banco de dados
                    db = CarroDB()
                    // deleta s carros antigos por tipo
                    db.deleteCarrosTipo(tipo)
                    for c in carros {
                        // sala o tipo do carro
                        c.tipo = tipo
                        //salva o carro no banco
                        db.save(c)
                    }
                    db.close()
                }
                // para retirba is dadis oeka funçao, vamos utilizar a theard principal
                dispatch_sync(dispatch_get_main_queue(),{
                    callback(carros:carros, error:error)
                })
 
            }
        })
        task.resume()
            

    }
    
    class func getCarrosByTipoFromFile(tipo: String) -> Array<Carro>{
        let file = "carros_" + tipo
        //let path = NSBundle.mainBundle().pathForResource(file, ofType: "xml")!
        let path = NSBundle.mainBundle().pathForResource(file, ofType: "json")!
        let data = NSData(contentsOfFile: path)!
        
        if (data.length == 0){
            print("NSData vazio")
            return []
        }
        
        //let carros = parserXML_SAX(data)
        //let carros = parserXML_DOM(data)
        let carros = parserJson(data)
        return carros
    }
    
    class func parserXML_SAX(data: NSData) -> Array<Carro>{
        if(data.length == 0){
            return []
        }
        
        var carros : Array<Carro> = []
        let xmlParser = NSXMLParser(data: data)
        let carrosParser = XMLCarroParser()
        xmlParser.delegate = carrosParser
        
        let ok = xmlParser.parse()
        if(ok){
            carros = carrosParser.carros
            let count = carros.count
            print("Parser, encontrado \(count) carros ")
        }else{
            print("Erro no parser")
        }
        
        return carros
    }
    
    // Parser de XML com DOM
    class func parserXML_DOM(date: NSData) -> Array<Carro> {
        var carros : Array<Carro> = []
        

        // Lê a tag raiz <carros>
        let document = try? SMXMLDocument(data: date)

        if(document == nil){
            print("ocorreu um erro no parser")
            return carros
        }
        
        let root = document!
        let tagCarros = root.childrenNamed("carro")
        
        // Percorre todas as tags <carro>
        for x:AnyObject in tagCarros  {
            let xml = x as! SMXMLElement
            
            let carro = Carro()
            carro.nome = xml.valueWithPath("nome")
            carro.desc = xml.valueWithPath("desc")
            carro.url_info = xml.valueWithPath("url_info")
            carro.url_foto = xml.valueWithPath("url_foto")
            carro.url_video = xml.valueWithPath("url_video")
            if(xml.valueWithPath("latitude") != nil) {
                carro.latitude = xml.valueWithPath("latitude")
            }
            if(xml.valueWithPath("longitude") != nil) {
                carro.longitude = xml.valueWithPath("longitude")
            }
            
            carros.append(carro)
            
        }

            
        
        return carros
    }
    
    class func parserJson(data: NSData) -> Array<Carro>{
        if(data.length == 0 ){
            return []
        }
        
        var carros : Array<Carro> = []
        
        let dict1 : NSDictionary = try! NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers) as! NSDictionary
        
        let jsonCarros : NSDictionary = dict1["carros"] as! NSDictionary
        let arrayCarros : NSArray = jsonCarros["carro"] as! NSArray
        
        for obj:AnyObject in arrayCarros{
            let dict2 = obj as! NSDictionary
            let carro = Carro()
            carro.nome = dict2["nome"] as! String
            carro.desc = dict2["desc"] as! String
            carro.url_foto  = dict2["url_foto"] as! String
            carro.url_info  = dict2["url_info"] as! String
            carro.url_video = dict2["url_video"] as! String
            carro.latitude  = dict2["latitude"]  as! String
            carro.longitude = dict2["longitude"] as! String
            carros.append(carro)
            
        }
        
        return carros
        
    }
    
}