//
//  DetalhesCarroViewController.swift
//  Carros
//
//  Created by Dauster on 12/10/15.
//  Copyright (c) 2015 Diogenes Dauster. All rights reserved.
//

import UIKit

class DetalhesCarroViewController: UIViewController {
    
    @IBOutlet var img : DownloadImagemView!
    @IBOutlet var tDesc : UITextView!
    
    var carro : Carro?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        if let c = carro{// testa se a variavel 'carro' nao esta nula e cria variavel 'c'
            self.title = c.nome
            self.tDesc.text = c.desc
            self.img.setUrl(c.url_foto)
            let btDeletar = UIBarButtonItem(title: "Deletar", style: UIBarButtonItemStyle.Plain, target: self, action: "onClickDeletar")
            self.navigationItem.rightBarButtonItem = btDeletar
        }
        
    }
    
    func onClickDeletar(){
        var alert = UIAlertController(title: "Confirma?", message: nil, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Destructive, handler: {(alert: UIAlertAction!) in self.deletar()}))
        alert.addAction(UIAlertAction(title: "Cancelar", style: UIAlertActionStyle.Destructive, handler: {(alert: UIAlertAction!) in }))
        
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    func deletar(){
        var db = CarroDB()
        db.delete(self.carro!)
        // Depois de deletar mostra um alert . Ao clicar no ok volta para a tela anteiror
        Alerta.alerta("Carro Excluido Com Sucesso !!", viewController: self, action: {
            (UIAlertAction) -> Void in
                self.goBack()
        })
        
    }
    
    func goBack(){
        //Fecha esta tela 
        self.navigationController!.popViewControllerAnimated(true)
    }
    
    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        print("viewWillTransitionToSize")
        if(size.width > size.height){
            print("Horizontal")
            tDesc.hidden = true
            // horizontal : esconde tudo
            self.tabBarController!.tabBar.hidden = true
            self.navigationController!.navigationBar.hidden = true
        }else{
            print("Vertical")
            tDesc.hidden = false
            // vertical : exibi tudo
            self.tabBarController!.tabBar.hidden = false
            self.navigationController!.navigationBar.hidden = false
        }
        // atualiza  o status da action bar
        self.setNeedsStatusBarAppearanceUpdate()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
