//
//  DownloadImageView.swift
//  Carros
//
//  Created by Diogenes Dauster on 08/11/15.
//  Copyright © 2015 Diogenes Dauster. All rights reserved.
//

import UIKit

class DownloadImagemView: UIImageView {
    // Para exibir a animação durante o download
    var progress: UIActivityIndicatorView!
    
    //Fila para fazer o download em background
    let queue = NSOperationQueue()
    
    // Fila principal para atualizar a interface
    let mainQueue = NSOperationQueue.mainQueue()
    
    // construtor
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        createProgress()
    }
 
    override init(frame: CGRect) {
        super.init(frame: frame)
        createProgress()
    }
    
    // UIActivityIndicatorView e adiciona por cima da imagem
    
    func createProgress(){
        progress = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.Gray)
        addSubview(progress)
    }
    
    override func layoutSubviews() {
        progress.center = convertPoint(self.center, fromView: self.superview)
    }
    
    // faz o download da URL, default é com cache
    
    func setUrl(url: String){
        setUrl(url,cache: true)
    }
    
    func setUrl(url:String,cache : Bool){
        self.image = nil
        queue.cancelAllOperations()
        //inicia a animação
        progress.startAnimating()
        // executa o download em background
        
        queue.addOperationWithBlock({self.downloadImg(url,cache: true)})
    }
    
    func downloadImg(url:String, cache : Bool){
        var data : NSData!
        
        if(!cache){
            // Download
            data = NSData(contentsOfURL: NSURL(string: url)!)!
        }else{
            // cria o caminho para ler ou salvar o arquivo
            var path = StringUtils.replace(url, string: "/", withString: "_")
            path = StringUtils.replace(url, string: "\\", withString: "_")
            path = StringUtils.replace(url, string: ":", withString: "_")
            path = NSHomeDirectory()+"/Documents/"+path
            
            // se o arquivo exite no cache
            
            let exists = NSFileManager.defaultManager().fileExistsAtPath(path)
            if(exists){
                // Lê o arquivo 
                data = NSData(contentsOfFile: path)
            }else{
                // Download 
                data = NSData(contentsOfURL: NSURL(string: url)!)!
                
                // salva o arquivo
                data.writeToFile(path, atomically: true)
            }
            
        }
        mainQueue.addOperationWithBlock({ self.showImg(data)})
    }
    
    
    func showImg(data: NSData){
        if(data.length > 0 ){
            self.image = UIImage(data: data)
        }
        
        progress.stopAnimating()
    }
}