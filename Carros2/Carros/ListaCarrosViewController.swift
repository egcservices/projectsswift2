//
//  ListaCarrosViewController.swift
//  Carros
//
//  Created by Dauster on 12/10/15.
//  Copyright (c) 2015 Diogenes Dauster. All rights reserved.
//

import UIKit

class ListaCarrosViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet var tableView: UITableView!
    @IBOutlet var progress : UIActivityIndicatorView!
    @IBOutlet var segmentControl: UISegmentedControl!
    
    var carros: Array<Carro> = []
    var tipo = "classicos"
    var cache = true

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        
        // titulo
        self.title = "Carros"
        // delegate
        self.tableView.dataSource = self
        self.tableView.delegate   = self
        // esssa linha é necessaria para usar o metodo dequeueResuableCellWithIderntifler mais abaixo
        //self.tableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: "cell")
        let xib = UINib(nibName: "CarroCell", bundle: nil)
        self.tableView.registerNib(xib, forCellReuseIdentifier: "cell")
        // Do any additional setup after loading the view.
        //self.carros = CarroService.getCarros()
        //self.carros = CarroService.getCarrosByTipoFromFile("esportivos")
        self.automaticallyAdjustsScrollViewInsets = false
        
        let btAtulizar = UIBarButtonItem(title: "Atualizar", style: UIBarButtonItemStyle.Plain, target: self, action: "atualizar")
        self.navigationItem.rightBarButtonItem = btAtulizar
        
        let idx = Prefs.getInteger("tipoIdx")
        let s = Prefs.getString("tipoString")
        
        if(s != nil){
            self.tipo = s
        }
        
        self.segmentControl.selectedSegmentIndex = idx
        
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        // busca carros
        self.buscarCarros()
    }
    
    func atualizar(){
        cache = false
        self.buscarCarros()
    }
    
    
    @IBAction func alterarTipo(sender: UISegmentedControl){
        let idx = sender.selectedSegmentIndex

        switch(idx){
            case 0:
            self.tipo = "classicos"
            case 1:
            self.tipo = "esportivos"
            default:
            self.tipo = "luxo"
        }
        
        Prefs.setInt(idx, chave: "tipoIdx")
        Prefs.setString(tipo, chave: "tipoString")
        
        self.buscarCarros()
    }
    
    func buscarCarros(){
        progress.startAnimating()
        let funcaoRetorno = {(carros: Array<Carro>,error: NSError!) -> Void in
            if(error != nil){
                Alerta.alerta("Erro: "+error!.localizedDescription, viewController: self)
            }else{
                self.carros = carros
                self.tableView.reloadData()
                self.progress.stopAnimating()
            }
        }
        CarroService.getCarrosByTipo(tipo,cache: cache, callback: funcaoRetorno)
        
        // bsucar os carros pelo tipo selecionado (classicos , esportivos , luxo)
        //self.carros = CarroService.getCarrosByTipoFromFile(tipo)
        //self.tableView.reloadData()
    }
    
    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask.Portrait
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section:Int) -> Int {
        return self.carros.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCellWithIdentifier("cell") as! CarroCell
        //let cell = self.tableView.dequeueReusableCellWithIdentifier("cell") as! UITableViewCell
        let linha = indexPath.row
        let carro = self.carros[linha]

        //cell.textLabel!.text = carro.nome
        //cell.imageView!.image = UIImage(named: carro.url_foto)
        
        
        cell.cellNome.text = carro.nome
        cell.cellDesc.text = carro.desc
        //let data = NSData(contentsOfURL: NSURL(string: carro.url_foto)!)!
        //cell.cellImg.image = UIImage(data: data)
        cell.cellImg.setUrl(carro.url_foto)
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let linha = indexPath.row
        let carro = self.carros[linha]
        //Alerta.alerta("Selecionou o carro: "+carro.nome, viewController: self)
        let vc = DetalhesCarroViewController(nibName:"DetalhesCarroViewController",bundle:nil)
        vc.carro = carro
        self.navigationController!.pushViewController(vc, animated: true)
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
