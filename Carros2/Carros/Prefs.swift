//
//  Prefs.swift
//  Carros
//
//  Created by Diogenes Dauster on 15/11/15.
//  Copyright © 2015 Diogenes Dauster. All rights reserved.
//

import UIKit

class Prefs {

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */
    
    class func getFilePath(nome: String) -> String {
        // Caminho com arquivo
        let path = NSHomeDirectory()+"/Documents/"+nome+".txt"
        return path
    }
    
    class func setString(valor: String, chave: String){
        //let prefs = NSUserDefaults.standardUserDefaults()
        //prefs.setValue(valor, forKey: chave)
        //prefs.synchronize()
        let path = Prefs.getFilePath(chave)
        let nsdata = StringUtils.toNSData(valor)
        _ = StringUtils.toString(nsdata)
        nsdata.writeToFile(path, atomically: true)
    }
    
    class func getString(chave: String) -> String! {
        //let prefs = NSUserDefaults.standardUserDefaults()
        //var s = prefs.stringForKey(chave)
        let path = Prefs.getFilePath(chave)
        let nsdata = NSData(contentsOfFile: path)
        let s = StringUtils.toString(nsdata)
        return s
    }
    
    class func setInt(valor: Int,chave: String){
        //let prefs = NSUserDefaults.standardUserDefaults()
        //prefs.setInteger(valor, forKey: chave)
        //prefs.synchronize()
        setString(String(valor), chave: chave)
    }
    
    class func getInteger(chave: String) -> Int! {
        //let prefs = NSUserDefaults.standardUserDefaults()
        //var s = prefs.integerForKey(chave)
        let valorString: String! = getString(chave)
        if(valorString == nil){
            return 0
        }
        let s:Int = Int(valorString)!
        return s

    }
    
    

}
