//
//  SQLiteHelper.swift
//  Carros
//
//  Created by Diogenes Dauster on 29/11/15.
//  Copyright © 2015 Diogenes Dauster. All rights reserved.
//

import Foundation


class SQLiteHelper : NSObject{
    var db: COpaquePointer = nil;
    
    init(database: String){
        super.init()
        self.db = open(database)
    }
    
    func getFilePath(name: String) -> String{
        // caminho do arquivo
        let path = NSHomeDirectory() + "/Documents/" + name + ".sqlite3"
        print("Database : \(path)")
        return path
    }
    
    func open(database: String) -> COpaquePointer{
        var db: COpaquePointer = nil;
        let path = getFilePath(database)
        let cPath = StringUtils.toCString(path)
        sqlite3_open(cPath,&db)
        return db
    }
    // executa o SQL
    func execSql(sql: String) -> CInt {
        return self.execSql(sql, params : nil)
    }
    //executa o SQL
    func execSql(sql : String , params: Array<AnyObject>!) -> CInt {
        var result : CInt = 0;
        _ = StringUtils.toCString(sql)
        // Statement
        let stmt = query(sql, params: params)
        // step
        result = sqlite3_step(stmt)
        if result != SQLITE_OK && result != SQLITE_DONE {
            sqlite3_finalize(stmt)
            let msg = "Erro ao executar SQL \n\(sql)\nError : \(lastSQLError())"
            print(msg)
            return -1
        }
        // se for ubser recupera o id
        if sql.uppercaseString.hasPrefix("INSERT") {
            // HTTP://WWW.SQLITE3.ORG/C3REF/LAST_INSERT_ROWIS.HTML
            let rid = sqlite3_last_insert_rowid(self.db)
            result = CInt(rid)
        }else{
            result = 1
        }
        // fecha statement
        sqlite3_finalize(stmt)
        return result
    }
    // faz bind dos parametros (?,?,?) de um SQL
    func bindParams(stmt: COpaquePointer , params : Array<AnyObject>!){
        if (params != nil){
            let size = params.count

            for i:Int in 1...size{
                let value : AnyObject = params[i-1]
                if(value is Int){
                    let number:CInt = toCInt(value as! Int)
                    sqlite3_bind_int(stmt, toCInt(i), number)
                }else{
                    let text : String = value as! String
                    // faz o bind por Objective-C
                    SQLiteObjc.bindText(stmt, idx: toCInt(i), withString: text)
                }
            }
        }
    }

    // executa o sql e retorna o statement
    func query(sql :String) -> COpaquePointer{
        return query(sql, params : nil)
        
    }
    // executa o sql e retorna o statement
    func query(sql :String, params: Array<AnyObject>!) -> COpaquePointer{
        var stmt: COpaquePointer = nil
        let cSql = StringUtils.toCString(sql)
        //prepare
        let result  = sqlite3_prepare_v2(self.db, cSql, -1, &stmt, nil)
        if result != SQLITE_OK {
            sqlite3_finalize(stmt)
            let msg = "Erro ao preparar SQL\n\(sql)\nError: \(lastSQLError())"
            print("SQLite ERROR \(msg)")
        }
        // Bind Values (?,?,?)
        if(params != nil){
            bindParams(stmt, params: params)
        }
        return stmt
    }
    
    // retorna true se existe a proxima linha da consulta 
    func nextRow(stmt: COpaquePointer) -> Bool {
        let result = sqlite3_step(stmt)
        let next: Bool = result == SQLITE_ROW
        return next
    }
    
    // Fecha o statement 
    func closeStatement(stmt : COpaquePointer){
        sqlite3_finalize(stmt)
    }
    
    // Fecha o banco de dados 
    
    func close(){
        sqlite3_close(self.db)
    }
    // Retorna o ultimo erro do SQL
    func lastSQLError() -> String {
        var err: UnsafePointer<Int8>? = nil
        err = sqlite3_errmsg(self.db)
        if(err != nil){
            let s = NSString(UTF8String: err!)
            return s! as String
        }
        return ""
    }
    
    // Le uma coluna do tipo Int
    func getInt(stmt:COpaquePointer, index: CInt) -> Int {
        let val = sqlite3_column_int(stmt, index)
        return Int(val)
    }
    // Le uma coluna do tipo Double
    func getDouble (stmt:COpaquePointer, index: CInt) -> Double {
        let val = sqlite3_column_double(stmt, index)
        return Double(val)
    }
    // Le uma coluna do tipo Float
    func getFloat (stmt:COpaquePointer, index: CInt) -> Float {
        let val = sqlite3_column_double(stmt, index)
        return Float(val)
    }
    // Le uma coluna do tipo Float
    func getString (stmt:COpaquePointer, index: CInt) -> String {
        let cString = SQLiteObjc.getText(stmt,idx: index)
        let s = String(cString)
        return s
    }
    // Converte Int (swift) para CInt(C)
    func toCInt(swiftInt: Int) -> CInt{
        let number: NSNumber = swiftInt as NSNumber
        let pos: CInt = number.intValue
        return pos
    }
    
    
    
}