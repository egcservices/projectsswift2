//
//  SQLiteObjc.h
//  Carros
//
//  Created by Diogenes Dauster on 29/11/15.
//  Copyright © 2015 Diogenes Dauster. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "sqlite3.h"
@interface SQLiteObjc : NSObject
+ (void) bindText:(sqlite3_stmt *) stmt idx:(int)idx withString:(NSString*)s;
+ (NSString *) getText:(sqlite3_stmt *) stmt idx:(int)idx;
@end