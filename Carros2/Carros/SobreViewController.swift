//
//  SobreViewController.swift
//  Carros
//
//  Created by Dauster on 12/10/15.
//  Copyright (c) 2015 Diogenes Dauster. All rights reserved.
//

import UIKit

let URL_SOBRE = "http://www.livroiphone.com.br/carros/sobre.htm"

class SobreViewController: UIViewController,UIWebViewDelegate {
    
    @IBOutlet var webView: UIWebView!
    @IBOutlet var progress:UIActivityIndicatorView!

    override func viewDidLoad() {
        super.viewDidLoad()
        // titulo
        self.title = "Sobre"
        // Do any additional setup after loading the view.
        // inicia a animaçao
        self.progress.startAnimating()
        // carregar url no webview
        let url = NSURL(string: URL_SOBRE)!
        let request = NSURLRequest(URL: url)
        self.webView.loadRequest(request)
        // delegate
        self.webView.delegate = self
        
        
    }
    
    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask.Portrait
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        // titulo
        self.title = "Sobre"
        // Do any additional setup after loading the view.
        // inicia a animaçao
        self.progress.startAnimating()
        // carregar url no webview
        let url = NSURL(string: URL_SOBRE)!
        let request = NSURLRequest(URL: url)
        self.webView.loadRequest(request)
        // delegate
        self.webView.delegate = self

    
    }
    
    func webViewDidFinishLoad(webView: UIWebView) {
        // para a animaçao
        progress.stopAnimating()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
