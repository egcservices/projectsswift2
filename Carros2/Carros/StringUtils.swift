//
//  StringUtils.swift
//  Carros
//
//  Created by Diogenes Dauster on 15/11/15.
//  Copyright © 2015 Diogenes Dauster. All rights reserved.
//

import UIKit

class StringUtils {
    
    class func toString(data: NSData!) -> String! {
        if(data == nil) {
            return nil
        }
        let s = NSString(data: data, encoding: NSUTF8StringEncoding)
        return s! as String
    }
    
    class func toNSData(s: String) -> NSData {
        let data = s.dataUsingEncoding(NSUTF8StringEncoding)
        return data!
    }
    
    class func toCString(s: String) -> UnsafePointer<Int8> {
        // cast to NSString
        // const char *
        let cstring = (s as NSString).UTF8String
        return cstring
    }
    
    class func trim(s: String) -> String {
        return s.stringByTrimmingCharactersInSet(.whitespaceAndNewlineCharacterSet())
    }
    
    class func isEmpty(s: String!) -> Bool {
        let count = StringUtils.count(s)
        return count == 0
    }
    
    class func count(s: String!) -> Int {
        if(s == nil) {
            return 0
        }
        let length = count(s)
        return length
    }
    
 
    class func replace(target: String,string: String,withString: String) -> String{
        let tmp : String = target
        return tmp.stringByReplacingOccurrencesOfString(string, withString: withString, options: NSStringCompareOptions.LiteralSearch, range: nil)

        
    }
    
}