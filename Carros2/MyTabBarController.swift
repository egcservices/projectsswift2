//
//  MyTabBarController.swift
//  Carros
//
//  Created by Dauster on 14/10/15.
//  Copyright (c) 2015 Diogenes Dauster. All rights reserved.
//

import UIKit

class MyTabBarController: UITabBarController {

    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        return self.selectedViewController!.supportedInterfaceOrientations()
    }

}
