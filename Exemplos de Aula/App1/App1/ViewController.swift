//
//  ViewController.swift
//  App1
//
//  Created by Elson Costa on 3/7/16.
//  Copyright (c) 2016 egcservice. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var txtCampo:UITextField?
    @IBOutlet weak var btnTest:UIButton?
    @IBOutlet weak var lblTeste:UILabel?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func exibirTexto()->Void{
        lblTeste?.text = txtCampo?.text
    }

}

