//
//  ViewController.swift
//  ElsonApp10GpsCoreLocation
//
//  Created by Bruno Rocha on 06/07/16.
//  Copyright (c) 2016 Bruno Rocha. All rights reserved.
//

import UIKit
import CoreLocation

class ViewController: UIViewController, CLLocationManagerDelegate {

    @IBOutlet weak var lblLat :UILabel!
    @IBOutlet weak var lblLong :UILabel!
    @IBOutlet weak var lblAlt :UILabel!
    @IBOutlet weak var lblPrecHor :UILabel!
    @IBOutlet weak var lblPrecVer :UILabel!
    @IBOutlet weak var lblDistPec :UILabel!
    
    var locMan : CLLocationManager!
    var locSaida : CLLocation!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        locMan = CLLocationManager()
        locMan.delegate = self
        locMan.desiredAccuracy = kCLLocationAccuracyBest
        locMan.requestAlwaysAuthorization()
    }
    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: NSDictionary?) -> Bool{
        return true
    }
    
    func locationManager(manager: CLLocationManager!, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        if (status == CLAuthorizationStatus.AuthorizedAlways){
            locMan.startUpdatingLocation()
        }else{
            println("Localização não foi capturada!")
        }
    }
    
    func locationManager(manager:CLLocationManager, didUpdateLocations locations:[AnyObject]){
        let newLoc : CLLocation = locations.last as! CLLocation
        if (locSaida == nil){
            locSaida = newLoc
        }
        lblLat.text = NSString(format: "%g \u{00B0}", newLoc.coordinate.latitude) as String
        lblLong.text = NSString(format: "%g \u{00B0}", newLoc.coordinate.longitude) as String
        lblAlt.text = NSString(format: "%g m", newLoc.altitude) as String
        lblPrecHor.text = NSString(format: "%g m", newLoc.horizontalAccuracy) as String
        lblPrecVer.text = NSString(format: "%g m", newLoc.verticalAccuracy) as String
        
        let distancia : CLLocationDistance = newLoc.distanceFromLocation(locSaida)
        lblDistPec.text = NSString(format: "%g m", distancia) as String
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

