//
//  ViewController.swift
//  ElsonApp11Camera
//
//  Created by Bruno Rocha on 06/07/16.
//  Copyright (c) 2016 Bruno Rocha. All rights reserved.
//

import UIKit
import MediaPlayer
import MobileCoreServices

class ViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var myImgView:UIImageView!
    @IBOutlet weak var myBtn:UIButton!
    
    var myImgFrame : CGRect!
    var myLastMediaType : String!
    var myMovieURL : NSURL!
    var myImg : UIImage!
    var myMoviePlayer:MPMoviePlayerController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        if(UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera) == false){
            //não há camera no dispositivo
            myBtn.hidden = true
        }
        
        myImgFrame = myImgView.frame
    }
    
    func getMediaFromControllerSourceType(sourceType:UIImagePickerControllerSourceType)->Void{
        let arrayMediaTypes : NSArray = UIImagePickerController.availableMediaTypesForSourceType(sourceType)!
        
        if(arrayMediaTypes.count > 0 && UIImagePickerController.isSourceTypeAvailable(sourceType)){
            let imagePickerController : UIImagePickerController = UIImagePickerController()
            imagePickerController.sourceType = sourceType
            imagePickerController.allowsEditing = true
            imagePickerController.mediaTypes = arrayMediaTypes as! [String]
            imagePickerController.delegate = self
            
            self.presentViewController(imagePickerController, animated: true, completion: nil)
        }else{
            print("não foi possivel utilizar esta opção")
        }
    }
    
    @IBAction func getMediaRealTime(){
        self.getMediaFromControllerSourceType(UIImagePickerControllerSourceType.Camera)
    }
    
    @IBAction func getMediaFromLibrary(){
        self.getMediaFromControllerSourceType(UIImagePickerControllerSourceType.PhotoLibrary)
    }

    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        picker.dismissViewControllerAnimated(true, completion: nil)
    }
    
   //func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [NSObject : AnyObject]) {
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        
    
        let infoDict : NSDictionary = info as NSDictionary
        
        myLastMediaType = infoDict.objectForKey(UIImagePickerControllerMediaType) as! String
        if (myLastMediaType == kUTTypeMovie as String){
           myMovieURL = infoDict.objectForKey(UIImagePickerControllerMediaURL) as! NSURL
        }else if (myLastMediaType == kUTTypeImage as String){
            myImg = infoDict.objectForKey(UIImagePickerControllerEditedImage) as! UIImage
        }
        
        picker.dismissViewControllerAnimated(true, completion: nil)
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        self.updateDisplay()
    }
    
    func updateDisplay() ->Void{
        if (myLastMediaType == nil){
            return
        }
        
        if(myLastMediaType == kUTTypeMovie as String){
            if(myMoviePlayer != nil){
                myMoviePlayer.view.removeFromSuperview()
            }
            
            myMoviePlayer = MPMoviePlayerController(contentURL: myMovieURL)
            myMoviePlayer.view.frame = myImgFrame
            myMoviePlayer.view.clipsToBounds = true
            self.view.addSubview(myMoviePlayer.view)
            myMoviePlayer.play()
            myImgView.hidden = true
        }
        else if(myLastMediaType == kUTTypeImage as String){
            myImgView.image = myImg
            myImgView.hidden = false
            
            if(myMoviePlayer != nil){
                myMoviePlayer.view.hidden = true
            }
            
        }
    }
}

