//
//  ViewController.swift
//  ElsonApp12Configuracoes
//
//  Created by Bruno Rocha on 12/07/16.
//  Copyright (c) 2016 Bruno Rocha. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var lblServidor : UILabel!
    @IBOutlet weak var lblPorta : UILabel!
    @IBOutlet weak var lblEmail : UILabel!
    @IBOutlet weak var lblSenha : UILabel!
    @IBOutlet weak var autoLogin : UILabel!
    @IBOutlet weak var lblModo : UILabel!
    @IBOutlet weak var lblEspaco : UILabel!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.getSettingsValue()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func actionToUpdate(){
        self.getSettingsValue()
    }
    
    func getSettingsValue(){
        var myUserDefaults = NSUserDefaults.standardUserDefaults()
        lblServidor.text = myUserDefaults.stringForKey("app_servidor")
        lblPorta.text = myUserDefaults.stringForKey("app_porta")
        lblEmail.text = myUserDefaults.stringForKey("app_email")
        lblSenha.text = myUserDefaults.stringForKey("app_senha")
        if (myUserDefaults.boolForKey("app_autologin") == true){
            autoLogin.text = "SIM"
        }else{
            autoLogin.text = "NÃO"
        }
        lblModo.text = myUserDefaults.stringForKey("app_modo")
        lblEspaco.text = myUserDefaults.stringForKey("app_slider")
    }
}

