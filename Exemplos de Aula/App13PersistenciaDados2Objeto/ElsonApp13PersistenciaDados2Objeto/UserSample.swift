//
//  UserSample.swift
//  ElsonApp13PersistenciaDados2Objeto
//
//  Created by Bruno Rocha on 13/07/16.
//  Copyright (c) 2016 Bruno Rocha. All rights reserved.
//

import Foundation

class UserSample:NSObject,NSCoding,NSCopying {
    var nome : String!
    var sobrenome: String!
    var email: String!
    
    func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encodeObject(nome, forKey: "nomeKey")
        aCoder.encodeObject(sobrenome, forKey: "sobrenomeKey")
        aCoder.encodeObject(email, forKey: "emailKey")
    }
    
    required init(coder aDecoder: NSCoder) {
        nome = aDecoder.decodeObjectForKey("nomeKey") as! String
        sobrenome = aDecoder.decodeObjectForKey("sobrenomeKey") as! String
        email = aDecoder.decodeObjectForKey("emailKey") as! String
    }
    
    override init(){
    
    }
    
    func copyWithZone(zone: NSZone) -> AnyObject{
        let myUserSample = UserSample()
        
        myUserSample.nome = self.nome
        myUserSample.sobrenome=self.sobrenome
        myUserSample.email=self.email
        
        return myUserSample
    }
    
    
//    func getNome() -> String{
//        return self.nome
//    }
//    
//    func setNome(nome: String)->Void{
//        self.nome = nome
//    }
    
}