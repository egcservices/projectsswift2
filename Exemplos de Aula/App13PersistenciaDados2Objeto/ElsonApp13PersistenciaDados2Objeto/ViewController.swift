//
//  ViewController.swift
//  ElsonApp13PersistenciaDados2Objeto
//
//  Created by Bruno Rocha on 13/07/16.
//  Copyright (c) 2016 Bruno Rocha. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var txtNome: UITextField!
    @IBOutlet weak var txtSobrenome: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    
    func getFilePath() -> String{
        let userDomainPaths : NSArray = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)
        let filepath = userDomainPaths.objectAtIndex(0) as! String
        return "\(filepath)encodedObjectFile"
    }
    
    func applicationWillResignActiveNotificationFunctions(notification: NSNotification) -> Void{
        let user = UserSample()
        user.nome = txtNome.text
        user.sobrenome = txtSobrenome.text
        user.email = txtEmail.text
        
        let data = NSMutableData()
        let archiver = NSKeyedArchiver(forWritingWithMutableData: data)
        archiver.encodeObject(user, forKey: "userKey")
        archiver.finishEncoding()
        data.writeToFile(self.getFilePath(), atomically: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        let filepath = self.getFilePath()
        if(NSFileManager.defaultManager().fileExistsAtPath(filepath)){
            let data = NSMutableData(contentsOfFile: filepath)!
            let unarchiver = NSKeyedUnarchiver(forReadingWithData: data)
            let user = unarchiver.decodeObjectForKey("userKey") as! UserSample
            unarchiver.finishDecoding()
            
            txtNome.text = user.nome
            txtSobrenome.text = user.sobrenome
            txtEmail.text = user.email
        }
        
        let myObj = UIApplication.sharedApplication()
        let mySelector : Selector = NSSelectorFromString("applicationWillResignActiveNotificationFunctions:")
        NSNotificationCenter.defaultCenter().addObserver(self,
            selector:mySelector,
            name:UIApplicationWillResignActiveNotification,
            object:myObj)       
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    


}

