//
//  ViewController.swift
//  ElsonApp14PersistenciaDados3CoreData
//
//  Created by Bruno Rocha on 13/07/16.
//  Copyright © 2016 Bruno Rocha. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController {
    
    @IBOutlet weak var nome :UITextField!
    @IBOutlet weak var sobrenome :UITextField!
    @IBOutlet weak var email :UITextField!
    
    var appDelegate: AppDelegate!
    var managedContext: NSManagedObjectContext!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        managedContext = appDelegate.managedObjectContext
        
        do
        {
            let myFetchRequest = NSFetchRequest(entityName: "Usuario")
            var myFetchedResults = try managedContext.executeFetchRequest(myFetchRequest) as! [NSManagedObject]
            
            if (!myFetchedResults.isEmpty){
                let user : NSManagedObject = myFetchedResults[0]
                nome.text = user.valueForKey("nome") as? String
                sobrenome.text = user.valueForKey("sobrenome") as? String
                email.text = user.valueForKey("email") as? String
                
                managedContext.deleteObject(user)
            }
        }
        catch
        {
            print("erro na leitura")
        }
        
        let myObj = UIApplication.sharedApplication()
        let mySelector : Selector = NSSelectorFromString("salvarDados:")
        NSNotificationCenter.defaultCenter().addObserver(self,
            selector: mySelector,
            name: UIApplicationWillResignActiveNotification,
            object: myObj)
    }
    
    func salvarDados(notification: NSNotification) -> Void{
        let entity = NSEntityDescription.entityForName("Usuario", inManagedObjectContext: managedContext)!
        let user = NSManagedObject(entity: entity, insertIntoManagedObjectContext: managedContext)
        
        user.setValue(nome.text, forKey: "nome")
        user.setValue(sobrenome.text, forKey: "sobrenome")
        user.setValue(email.text, forKey: "email")
        
        do
        {
            try managedContext.save()
        }
        catch
        {
            print("erro na gravação")
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

