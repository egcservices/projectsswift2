//
//  ViewController.swift
//  ElsonApp15DatePicker
//
//  Created by Bruno Rocha on 13/07/16.
//  Copyright © 2016 Bruno Rocha. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var myDatePicker: UIDatePicker!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func showInfo(){
        let myDate = myDatePicker.date as! NSDate
        print("A data é \(myDate)")
    }
    
}

