//
//  ViewController.swift
//  ElsonApp16PickerSingle
//
//  Created by Bruno Rocha on 13/07/16.
//  Copyright © 2016 Bruno Rocha. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {

    @IBOutlet weak var myPickerSingle : UIPickerView!
    var myPickerDataSource: NSArray!
    var id : Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.myPickerDataSource = NSArray(objects: "Cerveja", "Cachaça", "Whisky", "Suco", "Refri")
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func showInfo(){
        let myRow = myPickerSingle.selectedRowInComponent(0)
        let myValue = myPickerDataSource.objectAtIndex(myRow) as! String
        
        print("A bebida selecionada é \(myValue)")
    }
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int{
        return 1
    }
   
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        return myPickerDataSource.count
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return myPickerDataSource.objectAtIndex(row) as? String
    }
    
}

