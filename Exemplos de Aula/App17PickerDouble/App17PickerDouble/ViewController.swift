//
//  ViewController.swift
//  App17PickerDouble
//
//  Created by Bruno Rocha on 13/07/16.
//  Copyright © 2016 Bruno Rocha. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource{

    @IBOutlet weak var myPickerDouble : UIPickerView!
    var myPickerDataSourceTypes : NSArray!
    var myPickerDataSabores : NSArray!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        myPickerDataSourceTypes = NSArray(objects: "Cachaça", "Cerveja", "Suco", "Whisky")
        myPickerDataSabores = NSArray(objects:
            NSArray(objects: "Com Limão","Sem Limão"),
            NSArray(objects: "Clara", "Escura"),
            NSArray(objects: "Manga", "Uva", "Morango", "Graviola"),
            NSArray(objects: "Com água de coco", "Sem água de coco"))
        
        //(objects: "Manga", "Uva", "Morango", "Graviola")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int{
        return 2
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        if(component == 0){
            return myPickerDataSourceTypes.count
        }else{
            let myRow = myPickerDouble.selectedRowInComponent(0)
            let mySabores = myPickerDataSabores.objectAtIndex(myRow) as! NSArray
            return mySabores.count
        }
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if (component==0){
            return myPickerDataSourceTypes.objectAtIndex(row) as? String
        }else{
            let myRow = myPickerDouble.selectedRowInComponent(0)
            let mySabores = myPickerDataSabores.objectAtIndex(myRow) as! NSArray
            return mySabores.objectAtIndex(row) as? String
        }
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if (component==0){
            myPickerDouble.reloadComponent(1)
            myPickerDouble.selectRow(0, inComponent: 1, animated: true)
        }
    }

    @IBAction func showValue(){
        let myRow = myPickerDouble.selectedRowInComponent(0)
        let myValue = myPickerDataSourceTypes.objectAtIndex(myRow) as! String
        
        let myRowSab = myPickerDouble.selectedRowInComponent(1)
        let myValueSabs = myPickerDataSabores.objectAtIndex(myRow) as! NSArray
        let myValueSab = myValueSabs.objectAtIndex(myRowSab) as! String
        
        print("Meu tipo é: \(myValue) e a bebida foi \(myValueSab)")
    }
}

