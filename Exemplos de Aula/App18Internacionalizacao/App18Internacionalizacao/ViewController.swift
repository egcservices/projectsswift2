//
//  ViewController.swift
//  App18Internacionalizacao
//
//  Created by Bruno Rocha on 14/07/16.
//  Copyright © 2016 Bruno Rocha. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var lbl : UILabel!
    @IBOutlet weak var txt : UITextField!
    @IBOutlet weak var btn : UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        lbl.text = NSLocalizedString("txtLabel", comment: "")
        txt.placeholder = NSLocalizedString("txtTxt", comment: "")
        btn.setTitle(NSLocalizedString("txtBtn", comment: ""), forState:  UIControlState.Normal)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func work(){
        let myTitle = NSLocalizedString("titleAlert", comment: "") as String
        let myMsg = NSLocalizedString("msgAlert", comment: "") as String
        let myBtn = NSLocalizedString("btnAlert", comment: "") as String
        
        let myAcSheet = UIAlertController(title: myTitle, message: myMsg, preferredStyle: UIAlertControllerStyle.ActionSheet)
        let myAction = UIAlertAction(title: myBtn, style: UIAlertActionStyle.Cancel, handler: nil)
     
        myAcSheet.addAction(myAction)
        self.presentViewController(myAcSheet, animated: true, completion: nil)
    }
}

