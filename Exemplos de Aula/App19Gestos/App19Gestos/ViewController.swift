//
//  ViewController.swift
//  App19Gestos
//
//  Created by Bruno Rocha on 14/07/16.
//  Copyright © 2016 Bruno Rocha. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var lblStatus: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        let myLongPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: NSSelectorFromString("longPressEvent:"))
        myLongPressGestureRecognizer.minimumPressDuration = 2
        myLongPressGestureRecognizer.numberOfTapsRequired = 1
        self.view.addGestureRecognizer(myLongPressGestureRecognizer)
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: NSSelectorFromString("tapEvent:"))
        tapGestureRecognizer.numberOfTapsRequired = 2
        self.view.addGestureRecognizer(tapGestureRecognizer)
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: NSSelectorFromString("swipeLeftEvent:"))
        swipeLeft.direction = UISwipeGestureRecognizerDirection.Left
        self.view.addGestureRecognizer(swipeLeft)
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: NSSelectorFromString("swipeRightEvent:"))
        swipeRight.direction = UISwipeGestureRecognizerDirection.Right
        self.view.addGestureRecognizer(swipeRight)
        
        let swipeUp = UISwipeGestureRecognizer (target: self, action: NSSelectorFromString("swipeUpEvent:"))
        swipeUp.direction = UISwipeGestureRecognizerDirection.Up
        self.view.addGestureRecognizer(swipeUp)
        
        let swipeDown = UISwipeGestureRecognizer (target: self, action: NSSelectorFromString("swipeDownEvent:"))
        swipeDown.direction = UISwipeGestureRecognizerDirection.Down
        self.view.addGestureRecognizer(swipeDown)
        
        let rotation = UIRotationGestureRecognizer(target: self, action: NSSelectorFromString("rotationEvent:"))
        self.view.addGestureRecognizer(rotation)
        
        let pinch = UIPinchGestureRecognizer(target: self, action: NSSelectorFromString("pinchEvent:"))
        self.view.addGestureRecognizer(pinch)
    }
    
    //toque longo #sender de tipo CGPoint
    @IBAction func longPressEvent(sender: UIGestureRecognizer){
        let loc = sender.locationInView(self.view)
        lblStatus.text = "toque longo: \(loc.x), \(loc.y)"
    }
    
    //toque duplo rápido #sender de tipo CGPoint
    @IBAction func tapEvent(sender: UIGestureRecognizer)
    {
        let loc = sender.locationInView(self.view)
        lblStatus.text = "toque duplo rápido \(loc.x), \(loc.y)"
    }
    
    //gesto para esquerda
    @IBAction func swipeLeftEvent(sender: UIGestureRecognizer)
    {
        lblStatus.text = "toque realizado para esquerda"
    }
    
    //gesto para direita
    @IBAction func swipeRightEvent(sender: UIGestureRecognizer)
    {
        lblStatus.text = "toque realizado para direita"
    }
    
    //gesto para cima
    @IBAction func swipeUpEvent(sender: UIGestureRecognizer)
    {
        lblStatus.text = "toque realizado para cima"
    }
    
    //gesto para baixo
    @IBAction func swipeDownEvent(sender: UIGestureRecognizer)
    {
        lblStatus.text = "toque realizado para baixo"
    }
    
    //rotação #sender de tipo CGFloat
    @IBAction func rotationEvent(sender: UIRotationGestureRecognizer)
    {
        let rot = sender.rotation as CGFloat
        let vel = sender.velocity as CGFloat
        lblStatus.text = "Rotação: \(rot) \u{00B0} e Velocidade: \(vel)"
    }
    
    //gesto de pinça #sender de tipo CGFloat
    @IBAction func pinchEvent(sender: UIPinchGestureRecognizer)
    {
        let scale = sender.scale as CGFloat
        let vel = sender.velocity as CGFloat
        lblStatus.text = "Pinça: \(scale) e Velocidade: \(vel)"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

