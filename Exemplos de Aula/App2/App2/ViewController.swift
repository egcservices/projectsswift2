//
//  ViewController.swift
//  App2
//
//  Created by Elson Costa on 4/7/16.
//  Copyright (c) 2016 egcservice. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var txtCampo:UITextField?

    // SLIDER
    @IBOutlet weak var lblSlider:UILabel?
    @IBOutlet weak var mySlide:UISlider?
    
    // SWITCH
    @IBOutlet weak var mySwitch:UISwitch?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func msgAlert(texto:String!)->Void
    {
        let myActionSheet: UIAlertController=UIAlertController(title:"AVISO",message:texto,
            preferredStyle:UIAlertControllerStyle.ActionSheet)
        
        let myAction:UIAlertAction = UIAlertAction(title:"OK",style:UIAlertActionStyle.Default, handler: nil)
        myActionSheet.addAction(myAction)
        
        self.presentViewController(myActionSheet, animated:true,completion:nil)
    }
    
    @IBAction func showAlert(){
        msgAlert("Mensagem a ser exibida")
    }
    
    @IBAction func txtFieldReturn(sender:AnyObject){
        sender.resignFirstResponder()
    }

    @IBAction func backgroundTouch(){
        txtCampo?.resignFirstResponder()
    }
    
    @IBAction func showText(){
        msgAlert(txtCampo?.text)
    }
    
    @IBAction func valueChange(){
        var sliderFloat : Float!
        sliderFloat=mySlide?.value
        
        var sliderInt:Int
        sliderInt = Int(sliderFloat)
        
        var newlbl :String
        newlbl="\(sliderInt) %"
        
        lblSlider?.text = newlbl
    }
    
    @IBAction func showSwitch()
    {
        var myMsg : String
        if(mySwitch?.on == true)
        {
            myMsg = "Switch Ligado"
        }else{
            myMsg="Switch Desligado"
        }
        
        msgAlert(myMsg)
    }
    
    @IBAction func opcaoA(){
        println("Realizando a opção A")
    }
    
    @IBAction func opcaoB(){
        println("Realizando a opção B")
    }
    
    @IBAction func askConfirmation(){
        
        let myActionSheet: UIAlertController=UIAlertController(title:"Qual Ação você deseja realizar?",message:nil,
            preferredStyle:UIAlertControllerStyle.ActionSheet)
        
        let myActionA:UIAlertAction = UIAlertAction(title:"Opção A",style:UIAlertActionStyle.Default,
            handler: {(ACTION:UIAlertAction!)in self.opcaoA()})
        
        let myActionB:UIAlertAction = UIAlertAction(title:"Opção B",style:UIAlertActionStyle.Default,
            handler: {(ACTION:UIAlertAction!)in self.opcaoB()})
        
        let myAction:UIAlertAction = UIAlertAction(title:"Cancelar",style:UIAlertActionStyle.Cancel, handler: nil)
        
        myActionSheet.addAction(myActionA)
        myActionSheet.addAction(myActionB)
         myActionSheet.addAction(myAction)
        
        self.presentViewController(myActionSheet, animated:true,completion:nil)
    }
}

