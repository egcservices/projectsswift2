//
//  ViewController.swift
//  App20WebServices
//
//  Created by Bruno Rocha on 14/07/16.
//  Copyright © 2016 Bruno Rocha. All rights reserved.
//
// Documentação WebService
// http://apple.com/itunes/affiliates/resources/documentation/itunes-store-web-service-search-api.html

import UIKit

class ViewController: UIViewController {
    
    func searchItunesFor(buscarPor : String){
       let buscarPorFormat = buscarPor.stringByReplacingOccurrencesOfString(" ", withString: "+", options: NSStringCompareOptions.CaseInsensitiveSearch, range: nil)
        
        let buscarFinal = buscarPorFormat.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)!
        
        let urlWBS = "https://itunes.apple.com/search?term=\(buscarFinal)&media=music"
        
        let session = NSURLSession.sharedSession()
        let urlObj = NSURL(string: urlWBS)!
        
        let myTask = session.downloadTaskWithURL(urlObj, completionHandler:
            {
                (location, response, error) -> Void in
                
                if(error == nil){
                    let objData = NSData(contentsOfURL: location!)
                    
                    do{
                        let jsonData : NSDictionary = try NSJSONSerialization.JSONObjectWithData(objData!, options: NSJSONReadingOptions.MutableContainers) as! NSDictionary
                        let i = jsonData["resultCount"] as! Int
                        let results = jsonData["results"] as! NSArray
                        
                        for x in 0 ... i-1{
                            let res = results[x] as! NSDictionary
                            let nomeMus = res["trackName"] as! String
                            let preco = res["trackPrice"] as! Double
                            
                            print("\(nomeMus): U$ \(preco)")
                        }
                    }catch{
                        print("Erro na chamada JSON")
                    }
                }else {
                    print("Failed: \(error)")
                }
            })
        myTask.resume()
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        searchItunesFor("Linkin park")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

