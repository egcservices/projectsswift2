//
//  Categoria.swift
//  App21PortalWbs
//
//  Created by Bruno Rocha on 14/07/16.
//  Copyright © 2016 Bruno Rocha. All rights reserved.
//

import Foundation

class Categoria{
    private var id : Int!
    private var desc: String!

//    init(){}
//    
//    init(id:Int, desc:String){
//        self.id = id
//        self.desc = desc
//    }
    
    func getId() -> Int {
        return self.id
    }
    
    func setId(id :Int)->Void{
        self.id = id
    }
    
    func getDesc()->String{
        return self.desc
    }
    
    func setDesc(desc:String)->Void{
        self.desc = desc
    }
}
