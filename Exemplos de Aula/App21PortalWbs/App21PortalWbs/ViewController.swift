//
//  ViewController.swift
//  App21PortalWbs
//
//  Created by Bruno Rocha on 14/07/16.
//  Copyright © 2016 Bruno Rocha. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        listarCategoriasHttp()
//        
//        let myCats = listarCategoriasHttp()
//		
//        if(myCats.count > 0){
//            for var i = 0; i < myCats.count; i++ {
//              print(myCats[i].getDesc())
//            }
//        }else{
//            print("Sem informações")
//        }
	}

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func listarCategoriasHttp() -> Void{//[Categoria]{
        //var listaCategorias = [Categoria]()
        
        let urlWBS = "http://egcservice.com/webservices/portaldamoda/listar_categorias.php"
        
        let session = NSURLSession.sharedSession()
        let urlObj = NSURL(string: urlWBS)!
        
        let myTask = session.downloadTaskWithURL(urlObj, completionHandler:
            {
                (location, response, error) -> Void in
                
                if(error == nil){
                    let objData = NSData(contentsOfURL: location!)
                    
                    do{
                        let jsonData : NSDictionary = try NSJSONSerialization.JSONObjectWithData(objData!, options: NSJSONReadingOptions.MutableContainers) as! NSDictionary
                        
                        let results = jsonData["categorias_loja"] as! NSArray
                        
                        for x in 0 ... results.count-1{
                            let res = results[x] as! NSDictionary
                           
                            let idCat = res["ID"] as! String
                            let descCat = res["CATEGORIA_DESC"] as! String
                            
                            let cat = Categoria()
                            cat.setId(Int(idCat)!)
                            cat.setDesc(descCat)
                            print(cat.getDesc())
                            //listaCategorias.append(cat)
                        }
                    }catch{
                        print("Erro na chamada JSON")
                    }
                }else {
                    print("Failed: \(error)")
                }
        })
        myTask.resume()
        
        //return listaCategorias
    }
}

