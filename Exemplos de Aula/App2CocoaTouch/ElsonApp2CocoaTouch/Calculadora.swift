//
//  Calculadora.swift
//  ElsonApp2CocoaTouch
//
//  Created by Bruno Rocha on 30/06/16.
//  Copyright (c) 2016 Bruno Rocha. All rights reserved.
//

import Foundation

class Calculadora{
    private var num1:Float
    private var num2:Float
    
    init(num1 : Float, num2:Float)
    {
        self.num1 = num1
        self.num2 = num2
    }
    
    func getNum1()->Float{
        return self.num1
    }
    
    func setNum1(num1:Float)->Void
    {
        self.num1 = num1
    }
    
    func getNum2()->Float{
        return self.num2
    }
    
    func setNum2(num2:Float)->Void
    {
        self.num2 = num2
    }
    
    func calcular(acao:String, num1:Float, num2:Float) ->String{
        var result : Float
        
        switch(acao)
        {
        case "Som":
            result = num1+num2
            break
        case "Sub":
            result = num1-num2
            break
        case "Mult":
            result = num1*num2
            break
        case "Div":
            result = num1/num2
            break
        default:
            return ""
        }
        
        return "O resultado da operação foi \(result)"
    }
}