//
//  ViewController.swift
//  ElsonApp2CocoaTouch
//
//  Created by Bruno Rocha on 30/06/16.
//  Copyright (c) 2016 Bruno Rocha. All rights reserved.
//

import UIKit

class ViewController:UIViewController {
    
    @IBOutlet weak var lblOp:UILabel?
    @IBOutlet weak var lblResult:UILabel?
    @IBOutlet weak var txtNum1:UITextField!
    @IBOutlet weak var txtNum2:UITextField!
    @IBOutlet weak var btnSom:UIButton?
    @IBOutlet weak var btnSub:UIButton?
    @IBOutlet weak var btnMul:UIButton?
    @IBOutlet weak var btnDiv:UIButton?
    
    //outro exemplo
    @IBOutlet weak var txtOI:UITextField?
    @IBOutlet weak var btnCampo:UIButton?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func realizarOperacao(sender:UIButton)
    {
        let numberFormat = NSNumberFormatter()
        var btnClick:String = sender.titleForState(UIControlState.Normal)!
        var num1 : Float = numberFormat.numberFromString(txtNum1.text!)!.floatValue
        var num2 : Float = numberFormat.numberFromString(txtNum2.text!)!.floatValue
        var calc : Calculadora = Calculadora(num1: num1, num2: num2)
        lblResult?.text = calc.calcular(btnClick,num1:calc.getNum1(),num2:calc.getNum2())
        
        if (btnClick == "Som")
        {
            lblOp?.text = "+"
        }
        else if(btnClick=="Sub")
        {
            lblOp?.text = "-"
        }
        else if(btnClick=="Mult")
        {
            lblOp?.text = "*"
        }
        else if(btnClick=="Div")
        {
            lblOp?.text = "/"
        }
    }
    
    @IBAction func showAlert(){
        let myActionSheet : UIAlertController = UIAlertController(title:"AVISO", message:"Mensagem a ser exibida", preferredStyle:UIAlertControllerStyle.ActionSheet)
                let myAction : UIAlertAction = UIAlertAction(title:"OK", style: UIAlertActionStyle.Default, handler:nil)
                myActionSheet.addAction(myAction)
        
        self.presentViewController(myActionSheet, animated: true, completion: nil)
    }
    
    @IBAction func txtReturn(sender: AnyObject){
        //ele vai pegar o controle da aplicação e vai realocar para quem estava antes, ou seja ele fecha o teclado e volta pro formulário
        sender.resignFirstResponder()
    }
    
    @IBAction func backgroundTouch(){
        txtOI?.resignFirstResponder()
    }
    
    @IBAction func showText(){
        let myActionSheet : UIAlertController = UIAlertController(title:"VALOR DO CAMPO", message: txtOI?.text, preferredStyle:UIAlertControllerStyle.ActionSheet)
        let myAction : UIAlertAction = UIAlertAction(title:"OK", style: UIAlertActionStyle.Default, handler:nil)
        myActionSheet.addAction(myAction)
        
        self.presentViewController(myActionSheet, animated: true, completion: nil)    }
}

