//
//  ViewController.swift
//  App3
//
//  Created by Elson Costa on 4/7/16.
//  Copyright (c) 2016 egcservice. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        var texto : String
        var inteiro: Int
        var decimal: Float
        var decimalDouble : Double
        
        // Int para String
        inteiro = 10
        texto = "\(inteiro)"
        texto = String(inteiro)
        println(texto)

        // Float-Double para String
        decimal = 5.55
        texto =  "\(decimal)"
        println(texto)
        
        texto = String(format:"%f",decimal)
        println(texto)
        
        //String para Int
        texto = "7"
        inteiro = texto.toInt()! //versão 9<
        //inteiro = Int(texto)! //versão >= 9
        inteiro++
        println(inteiro)
        
        //String para Float
        texto = "1.234"
        var textoNS:NSString = texto
        decimal = textoNS.floatValue
        decimal++
        println(decimal)
        
        //String para Double
        decimalDouble = textoNS.doubleValue
        decimalDouble++
        println(decimalDouble)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

