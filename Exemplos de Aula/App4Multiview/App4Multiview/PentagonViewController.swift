//
//  PentagonViewController.swift
//  App4Multiview
//
//  Created by Elson Costa on 5/7/16.
//  Copyright (c) 2016 egcservice. All rights reserved.
//

import UIKit
import Foundation

class PentagonViewController:UIViewController{
    
    @IBOutlet weak var myTxt :UITextField?
    //@IBOutlt weak var mytn
    
    override func viewDidLoad(){
    
    }
    
    @IBAction func txtFieldReturn(sender:AnyObject){
        sender.resignFirstResponder()
    }
    
    @IBAction func bcTouch(){
        myTxt?.resignFirstResponder()
    }
    
    
    @IBAction func sendValue(){
        let myAppDeleg = UIApplication.sharedApplication().delegate as AppDelegate;
        myAppDeleg.sharedValue = myTxt?.text
    }
    
}