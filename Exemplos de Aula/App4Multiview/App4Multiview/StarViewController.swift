//
//  StarViewController.swift
//  App4Multiview
//
//  Created by Elson Costa on 5/7/16.
//  Copyright (c) 2016 egcservice. All rights reserved.
//

import UIKit
import Foundation

class StarViewController:UIViewController{
    
    @IBOutlet weak var myLbl : UILabel?
    
    override func viewDidLoad(){
        
    }
    
    @IBAction func updateValue(){
        let myAppDeleg = UIApplication.sharedApplication().delegate as AppDelegate
        myLbl?.text = myAppDeleg.sharedValue
    }
}