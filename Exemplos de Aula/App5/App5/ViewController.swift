//
//  ViewController.swift
//  App5
//
//  Created by Elson Costa on 5/7/16.
//  Copyright (c) 2016 egcservice. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func abrirNovaJanela(){
        var nj : NovaJanelaViewController = NovaJanelaViewController(nibName:"NovaJanelaViewController",bundle:nil)
        self.presentViewController(nj, animated: true, completion:nil)
        
    }

}

