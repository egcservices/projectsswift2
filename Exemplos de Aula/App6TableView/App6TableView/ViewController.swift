//
//  ViewController.swift
//  App6TableView
//
//  Created by Elson Costa on 5/7/16.
//  Copyright (c) 2016 egcservice. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {

    var myDtSource:NSMutableArray = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    
        for i in 0...10
        {
            let tempNome : String = "Elson \(i)"
            let tempSobreNome : String = "Costa \(i)"
            let tempUser:NSArray = NSArray(objects: tempNome,tempSobreNome)
            self.myDtSource.addObject(tempUser)
        }
    
    }
    
    func tableView(tableView : UITableView, numberOfRowsInSection: Int)->Int{
        return self.myDtSource.count
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath:NSIndexPath) {
        let myObj : NSArray = self.myDtSource.objectAtIndex(indexPath.row) as! NSArray
        let myMsg : String = (myObj.objectAtIndex(0) as! String) + (myObj.objectAtIndex(1) as! String)
        
        println(myMsg)
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let myId : String = "exemplo"
        
        var cell : UITableViewCell! = tableView.dequeueReusableCellWithIdentifier(myId) as! UITableViewCell!
        
        if (cell == nil){
            cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: myId)
        }
        
        let myObj:NSArray = self.myDtSource.objectAtIndex(indexPath.row) as! NSArray
        cell.textLabel?.text = myObj.objectAtIndex(0) as? String
        cell.detailTextLabel?.text = myObj.objectAtIndex(1) as? String
        
        return cell
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    


}

