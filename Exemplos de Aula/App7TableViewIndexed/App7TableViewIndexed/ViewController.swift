//
//  ViewController.swift
//  App7TableViewIndexed
//
//  Created by Elson Costa on 5/7/16.
//  Copyright (c) 2016 egcservice. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    var musGroups : NSDictionary = NSDictionary()
    var keysMusGr : NSMutableArray = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        let filepath : String = NSBundle.mainBundle().pathForResource("Bandas", ofType: "plist")!
        musGroups = NSDictionary(contentsOfFile: filepath)!
        
        keysMusGr = NSMutableArray(array: musGroups.allKeys)
        
        keysMusGr.sortUsingSelector(NSSelectorFromString("compare:"))
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int)->Int
    {
        let myKey:String = keysMusGr.objectAtIndex(section) as! String
        let secList: NSArray = musGroups.objectForKey(myKey) as! NSArray
        return secList.count
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return keysMusGr.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let myId:String = "id"
        
        var cell : UITableViewCell! = tableView.dequeueReusableCellWithIdentifier(myId) as! UITableViewCell!
        
        if (cell == nil){
            cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: myId)
        }
        
        let section : Int = indexPath.section
        let row: Int = indexPath.row
        
        let key : String = keysMusGr.objectAtIndex(section) as! String
        let groups : NSArray = musGroups.objectForKey(key) as! NSArray
        let musGr : String = groups.objectAtIndex(row) as! String
        
        cell.textLabel?.text = musGr
        
        return cell
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

