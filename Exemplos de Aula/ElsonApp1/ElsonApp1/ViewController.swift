//
//  ViewController.swift
//  ElsonApp1
//
//  Created by Bruno Rocha on 27/06/16.
//  Copyright (c) 2016 Bruno Rocha. All rights reserved.
//

import UIKit


let myConstantGlobalPI : Float = 3.14

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        print("Exemplos de Operações Matématicas e Lógicas")
        
        // STRING
        
        var myString : String = String()
        myString = "Texto aqui"
        println(myString)
        
        // INT
        let x, y : Int
        x = 6
        y = 2
        println("O resultado da soma é:  \(x+y)")
        println("O resultado da subtração é: \(x-y)")
        println("O resultado da multiplicação é: \(x*y)")
        println("O resultado da divisão é: \(Float(x)/Float(y))")
        
        // FLOAT
        var dobroPi :String = "O dobro de Pi é: \(myConstantGlobalPI * 2)"
        println(dobroPi)
        
        //FOR
        println("For: ")
        var i : Int
        for i=0;i<=5;i++
        {
            println(i)
        }
        
        println("Outro For: ")
        i=0
        for i in 0...5
        {
            if (i == 4)
            {
                continue
            }
            println("For mais simplificado: \(i)")
        }
        
        println("Mais outro For: ")
        i=0
        for i in 0...5
        {
            if (i == 2)
            {
                println("Vai parar! ˜ Parou")
                break
            }
            println(i)
        }
        
        //IF
        
        println("If: ")
        i = 5
        if (i==3){
            println("i é igual a \(i)")
        }else if (i==5){
            println("i é igual a 5")
        }else{
            println("i não é igual a 5")
        }
        
        //SHORT IF
        println("Short If: ")
        i = 3
        myString = i > 5 ? "i é maior que 5":"i é menor ou igual a 5"
        println(myString)
        
        //SWITCH/CASE
        println("Switch / Case")
        i = 1
        switch(i)
            {
        case 1:
            println("entrou no case 1")
            break
        case 2:
            println("entrou no case 2")
            break
        case 3:
            println("entrou no case 3")
            break
        default:
            println("não entrou em nenhum case")
            break
        }
        
        // FUNÇÕES
        i = 3
        let w : Int = retornaDobro(i)
        println("O dobro de \(i) é \(w)")
        
        //DADOS AVANÇADOS
        //NSInteger
        let ins : NSInteger = 50
        println("NSInteger: \(ins)")
        
        //NSNumber
        let nns: NSNumber = 12.10
        println("NSNumber: \(nns)")
        
        //Array
        //var myArray:[String] = ["Vermelho", "Verde", "Azul", "Rosa"]
        //var myArray:NSArray = ["Vermelho", "Verde", "Azul", "Rosa"]
        
        var myArray:NSMutableArray = ["Vermelho", "Verde", "Azul", "Rosa"]
        
        println("A posição 1 do meu array é: \(myArray[1])")
        var total : Int = myArray.count
        println("Total dos meus arrays é: \(total)")
        for cor in myArray{
            println("Meu Array é: \(cor)")
        }
        
        myArray[0] = "Cinza"
        myArray.removeObjectAtIndex(1)
        myArray.insertObject("Amarelo", atIndex: total-1)
        
        for cor in myArray{
            println("Meu Array atual é: \(cor)")
        }
        
        //NSDictionary
        println("Usando NSDictionary")
        let myDictionary : NSDictionary = ["chave1": "valor1", "chave2":"valor2","chave3":"valor3"]
        let myKey : String = "chave3"
        let myObject : AnyObject
        myObject = myDictionary.objectForKey(myKey)!
        
        println("A chave \(myKey) está ligada diretamente ao valor \(myObject) do NSDictionary")
    }
    
    func retornaDobro(valor:Int) -> Int{
        return valor * 2
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

