//
//  ViewController.swift
//  SegmentedControlTableView
//
//  Created by Bruno Rocha on 12/07/16.
//  Copyright (c) 2016 Bruno Rocha. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var mySegmentControl : UISegmentedControl!
    @IBOutlet weak var myTableView : UITableView!
    
    var myList1 : [String] = ["List1_222", "List1_3122", "List1_2141"]
    var myList2 : [String] = ["List2_rweds", "List2_fsdfds", "List2_dasdcx", "List2_zasnczxn"]
    var myList3 : [String] = ["List3_tvxfb", "List3_vxcvcx", "List3_djasd","List3_sadsadasdcx", "List3_34asdsazasnczxn"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        var returnValue = 0
      
        switch (mySegmentControl.selectedSegmentIndex){
        case 0:
            returnValue = myList1.count
            break
        case 1:
            returnValue = myList2.count
            break
        case 2:
            returnValue = myList3.count
            break
        default:
            break
        }
        
        return returnValue
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let myCell = tableView.dequeueReusableCellWithIdentifier("myCell", forIndexPath: indexPath) as! UITableViewCell
        
        switch (mySegmentControl.selectedSegmentIndex){
        case 0:
            myCell.textLabel?.text = myList1[indexPath.row]
            break
        case 1:
            myCell.textLabel?.text = myList2[indexPath.row]
            break
        case 2:
            myCell.textLabel?.text = myList3[indexPath.row]
            break
        default:
            break
        }
        
        return myCell
    }
    
    @IBAction func mySegmentChanged(sender:AnyObject)
    {
        myTableView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

