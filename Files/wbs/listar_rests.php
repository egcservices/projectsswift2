<?php
	include_once("Conexao.php");

	try{

		if(!$conectou){
			//nfpc: Não foi possível conectar
			throw new Exception("nfpc");
		}

		$query = "SELECT * FROM tb_restaurante;";
		
		$res = mysql_query($query) or die(mysql_error());
		$num_rows = mysql_num_rows($res);
		$contador = 0;

		// while($linha = mysql_fetch_array($res)){
		// 	$meuArray = array('id' => $linha['id'], 'user' => $linha['id_user'], 'text' => $linha['text_post'], 'data' => $linha['data_hora_post']);
		// }
		// echo json_encode($meuArray);

		echo "{";
			echo '"restaurantes":';
				echo "[";
				
				while($linha =  mysql_fetch_array($res)){
					$contador++;
					if ($linha) {
						echo "{";
							//echo '"success": ' 				. '"1"'									  . ", ";
							echo '"id": '    	   			. '"' . $linha['id']    		 	. '"' . ", ";
                            echo '"nome": '    	   	        . '"' . $linha['nome']    			. '"' . ", ";
                            echo '"endereco": '    	   	    . '"' . $linha['endereco']    		. '"' . ", ";
                            echo '"latitude": '    	   	    . '"' . $linha['latitude']    		. '"' . ", ";
                            echo '"longitude": '    	   	. '"' . $linha['longitude']    		. '"' . ", ";
							echo '"kms": '    	            . '"' . $linha['km_permitidos']	    . '"';
						
						if ($contador == $num_rows ) {
							echo "}";
						} else {
							echo "},";
						}
					}
				}
			echo "]";
		echo "}";
		
	}catch(Exception $ex){

		$response = array();	

		switch ($ex->getMessage()) 
		{
			case 'nfpc':
				$response["success"] = "0";
				$response["message"] = "Não foi possivel conectar. ERROR: " . mysql_error();
				break;

			case 'dnp':
				$response["success"] = "0";
				$response["message"] = "Dados não preenchidos. ERROR: " . mysql_error();
				break;

			default:
				break;
		}

		echo json_encode($response);
	}
?>