//
//  Contato.swift
//  MigrationDbWeb
//
//  Created by Bruno Rocha on 21/07/16.
//  Copyright © 2016 Bruno Rocha. All rights reserved.
//

import Foundation
import CoreData

extension Contato {
    @NSManaged var nome: String?
    @NSManaged var email: String?
}

class Contato: NSManagedObject {
    static let entityName = "Contato"
}