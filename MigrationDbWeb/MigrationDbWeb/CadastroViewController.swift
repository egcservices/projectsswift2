//
//  CadastroViewController.swift
//  MigrationDbWeb
//
//  Created by Bruno Rocha on 15/07/16.
//  Copyright © 2016 Bruno Rocha. All rights reserved.
//

import UIKit
import CoreData

class CadastroViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var txtNome : UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var btnOk : UIButton!
    @IBOutlet weak var btnEnviarWeb :UIButton!
    @IBOutlet weak var lblInfoCad:UILabel!
    @IBOutlet weak var icon:UIImageView!
    @IBOutlet weak var myTable:UITableView!
    
    var managedContext: NSManagedObjectContext!
    var contatoService : ContatoServices!
    var contatos = [Contato]()
    var myActivityIndicator = UIActivityIndicatorView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lblInfoCad.hidden = true
        configurarComponentes()
        carregarDbLocal()
    }
    
    func configurarComponentes(){
        btnOk.layer.cornerRadius = 4;
        btnOk.layer.borderWidth = 1;
        btnOk.layer.borderColor = UIColor.blackColor().CGColor
        btnOk.backgroundColor = UIColor.whiteColor()
        btnOk.tintColor = UIColor.blackColor()
        
        btnEnviarWeb.layer.cornerRadius = 4;
        btnEnviarWeb.layer.borderWidth = 1;
        btnEnviarWeb.layer.borderColor = UIColor.blackColor().CGColor
        btnEnviarWeb.backgroundColor = UIColor.whiteColor()
        btnEnviarWeb.tintColor = UIColor.blackColor()
        
        myActivityIndicator = UIActivityIndicatorView(frame: CGRectMake(0,0,50,50))
        myActivityIndicator.center = self.view.center
//        myActivityIndicator.hidesWhenStopped = true
        myActivityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.Gray
        self.view.addSubview(myActivityIndicator)
    }
    
    func carregarDbLocal(){
        activityInSwitch(true)
        managedContext = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
        contatoService = ContatoServices(context: managedContext)
        self.contatos = contatoService.listarContatos()
        activityInSwitch(false)
    }
    
    func activityInSwitch(ligar:Bool){
        myActivityIndicator.hidesWhenStopped = true
        self.view.addSubview(myActivityIndicator)

        if (ligar){
            self.myActivityIndicator.startAnimating()
            UIApplication.sharedApplication().beginIgnoringInteractionEvents()
        }else{
            self.myActivityIndicator.stopAnimating()
            UIApplication.sharedApplication().endIgnoringInteractionEvents()
        }
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func ValidarCampos() -> Bool{
        if (txtNome.text == "" || txtEmail.text == "" ){
            return false
        }
        return true
    }
    
    func ValidarEmail(enteredEmail: String)->Bool{
        let emailFormat = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailFormat)
        return emailPredicate.evaluateWithObject(enteredEmail)
    }
    
    @IBAction func cadastrar(){
        if (ValidarCampos() && ValidarEmail(txtEmail.text!)){
            let cNome = txtNome.text!
            let cEmail = txtEmail.text!
            //let contatoSalvo = 
            contatoService.cadastrarContato(cNome, email:cEmail)
            if (saveChanges() == "1"){
                txtNome.text = ""
                txtEmail.text = ""
                if (self.contatos.count > 0){
                    self.btnEnviarWeb.enabled = true
                    self.lblInfoCad.hidden = true
                }
                
                dispatch_async(dispatch_get_main_queue(), {
                    self.myTable.reloadData()
                })
            }
            else{
                exibirAlert("Erro", msg:"Erro ao salvar o contato!", nameBtn:"Ok")
            }
        }else{
            exibirAlert("Verificar Campos", msg:"Verifique os campos de Nome e Email!", nameBtn:"Continuar")
        }
    }
    
    func exibirAlert(titleAlert: String, msg: String, nameBtn:String){
        let myActionSheet = UIAlertController(title: titleAlert, message: msg, preferredStyle:UIAlertControllerStyle.ActionSheet)
        myActionSheet.addAction(UIAlertAction(title: nameBtn, style: UIAlertActionStyle.Default, handler:nil))
        self.presentViewController(myActionSheet, animated: true, completion: nil)
    }
    
    @IBAction func migrarWeb(sender:AnyObject){
        self.activityInSwitch(true)
        self.btnEnviarWeb.enabled = false
        self.btnEnviarWeb.setTitle("Enviando...", forState: UIControlState.Disabled)

        //let myQueue = dispatch_get_global_queue(QOS_CLASS_BACKGROUND, 0)
        //dispatch_async(myQueue, {
            for cont in self.contatos {
                let cNome = cont.nome!
                let cEmail = cont.email!
                
                let urlWBS = NSURL(string:"http://egcservice.com/webservices/contatos/salvar_contato.php?nome=\(self.FormatValue(cNome))&email=\(self.FormatValue(cEmail))")!
                let request = NSMutableURLRequest(URL:urlWBS);
                
                let task = NSURLSession.sharedSession().dataTaskWithRequest(request) {
                    data, response, error in
                    
                    if(error == nil){
                        do{
                            let myJSON =  try NSJSONSerialization.JSONObjectWithData(data!, options: .MutableContainers) as? NSDictionary
                            
                            if let parseJSON = myJSON {
                                let result = parseJSON["success"] as! Int
                                if (result == 1){
                                    let coDel = self.contatoService.getContatoById(cont.objectID)!
                                    self.contatoService.removerContato(coDel.objectID)
                                    self.saveChanges()
                                }
                            }
                        }catch{
                            self.exibirAlert("Erro!", msg:"Erro ao migrar o contato", nameBtn:"Ok")
                        }
                    }else {
                        self.exibirAlert("Erro!", msg:"Erro ao migrar o contato", nameBtn:"Ok")
                    }
                }
                task.resume()
            }
            
            // stop when task finishes
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    self.contatos.removeAll()
                    self.btnEnviarWeb.setTitle("Enviar Web", forState: UIControlState.Disabled)
                    self.activityInSwitch(false)
                    self.myTable.reloadData()
                
            })

        //})
    }
    
    func FormatValue(value: String) -> String{
        let valueFormat = value.stringByReplacingOccurrencesOfString(" ", withString: "+", options: NSStringCompareOptions.CaseInsensitiveSearch, range: nil)
         return valueFormat.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!
        //return valueFormat.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)! //deprecated
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        if (self.contatos.count > 0){
            self.lblInfoCad.hidden = true
//            self.btnEnviarWeb.enabled = true
//            self.btnEnviarWeb.setTitle("Enviar Web", forState: .Disabled)
        }else{ //if (self.contatos.count == 0){
            self.btnEnviarWeb.enabled = false
            self.lblInfoCad.hidden = false
            self.lblInfoCad.text = "Não foram encontrados contatos salvos!"
        }
        return contatos.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell{
        let myCell = tableView.dequeueReusableCellWithIdentifier("myCell", forIndexPath: indexPath)
        let contato = contatos[indexPath.row]
        
        let cNome = contato.nome!
        let cEmail = contato.email!
        
        myCell.textLabel?.numberOfLines = 2
        //myCell.textLabel?.textAlignment = NSTextAlignment.Center
        myCell.textLabel?.lineBreakMode = NSLineBreakMode.ByWordWrapping
        myCell.imageView?.image = UIImage(named: "icon_register")
        myCell.textLabel!.text = cNome + "\n" + cEmail
        return myCell
    }
    
    override func viewDidAppear(animated: Bool) {
        self.myTable.reloadData()
    }
    
    
    func saveChanges() -> String{
        do{
            try self.managedContext.save()
            carregarDbLocal()
            self.myTable.reloadData()
            return "1"
        }catch{
            return "0"
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

/*
@IBAction func showAlert(){
let myActionSheet : UIAlertController = UIAlertController(title:"AVISO", message:"Mensagem a ser exibida", preferredStyle:UIAlertControllerStyle.ActionSheet)
let myAction : UIAlertAction = UIAlertAction(title:"OK", style: UIAlertActionStyle.Default, handler:nil)
myActionSheet.addAction(myAction)
self.presentViewController(myActionSheet, animated: true, completion: nil)
}

@IBAction func txtReturn(sender: AnyObject){
//ele vai pegar o controle da aplicação e vai realocar para quem estava antes, ou seja ele fecha o teclado e volta pro formulário
sender.resignFirstResponder()
}

@IBAction func backgroundTouch(){
txtOI?.resignFirstResponder()
}

/salvar json
// prepare json data
let mapDict = [ "1":"First", "2":"Second"]

let json = [ "title":"ABC" , "dict": mapDict ]
let jsonData = NSJSONSerialization.dataWithJSONObject(json, options: .PrettyPrinted, error: nil)

// create post request
let url = NSURL(string: "http://httpbin.org/post")!
let request = NSMutableURLRequest(URL: url)
request.HTTPMethod = "POST"

// insert json data to the request
request.HTTPBody = jsonData


let task = NSURLSession.sharedSession().dataTaskWithRequest(request){ data,response,error in
if error != nil{
println(error.localizedDescription)
return
}
if let responseJSON = NSJSONSerialization.JSONObjectWithData(data, options: nil, error: nil) as? [String:AnyObject]{
println(responseJSON)
}
}

task.resume()


//            let indicator: UIActivityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.Gray)
//            myProgress.frame = CGRectMake(0.0, 0.0, 40.0, 40.0);
//            myProgress.center = view.center
//            myProgress.tintColor = UIColor.blackColor()
//            view.addSubview(myProgress)
//            myProgress.bringSubviewToFront(view)
//            UIApplication.sharedApplication().networkActivityIndicatorVisible = true
*/
