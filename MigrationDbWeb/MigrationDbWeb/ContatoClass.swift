//
//  Contato.swift
//  MigrationDbWeb
//
//  Created by Bruno Rocha on 21/07/16.
//  Copyright © 2016 Bruno Rocha. All rights reserved.
//

import Foundation

class ContatoClass{
    private var nomeContato : String!
    private var emailContato: String!
    
    func getNomeContato()->String{
        return self.nomeContato
    }
    
    func getEmailContato()->String{
        return self.emailContato
    }
    
    func setNomeContato(nome:String){
        self.nomeContato = nome
    }
    
    func setEmailContato(email:String){
        self.emailContato = email
    }
}
