//
//  Contato.swift
//  MigrationDbWeb
//
//  Created by Bruno Rocha on 21/07/16.
//  Copyright © 2016 Bruno Rocha. All rights reserved.
//

import Foundation
import CoreData

class ContatoServices{
    
    var context: NSManagedObjectContext
    
    init(context: NSManagedObjectContext){
        self.context = context
    }
    
    func cadastrarContato(nome:String, email:String) -> Contato {
        let contatoCad = NSEntityDescription.insertNewObjectForEntityForName(Contato.entityName, inManagedObjectContext: context) as! Contato
        
        contatoCad.nome = nome
        contatoCad.email = email
        
        return contatoCad
    }
    
    // Gets all with an specified predicate.
    // Predicates examples:
    // - NSPredicate(format: "name == %@", "Juan Carlos")
    // - NSPredicate(format: "name contains %@", "Juan")
    func listarContatos() -> [Contato]{
        let myFetchRequest = NSFetchRequest(entityName: Contato.entityName)
        do {
            let response = try context.executeFetchRequest(myFetchRequest)
            return response as! [Contato]
        } catch let error as NSError {
            // failure
            print(error)
            return [Contato]()
        }
    }
    
    func getContatoById(id: NSManagedObjectID) -> Contato? {
        return context.objectWithID(id) as? Contato
    }
    
    func atualizarContato(newContato : Contato){
        if let contato = getContatoById(newContato.objectID){
            contato.nome = newContato.nome
            contato.email = newContato.email
        }
    }
    
    func removerContato(id: NSManagedObjectID){
        if let contatoDel = getContatoById(id){
            context.deleteObject(contatoDel)
        }
    }
}
