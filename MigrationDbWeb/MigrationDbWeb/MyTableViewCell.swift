//
//  MyTableViewCell.swift
//  MigrationDbWeb
//
//  Created by Bruno Rocha on 20/07/16.
//  Copyright © 2016 Bruno Rocha. All rights reserved.
//

import UIKit

class MyTableViewCell: UITableViewCell {

    @IBOutlet weak var imgPic : UIImageView!
    @IBOutlet weak var lblNome: UILabel!
    @IBOutlet weak var lblEmail : UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
