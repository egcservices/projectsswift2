//
//  WebViewController.swift
//  MigrationDbWeb
//
//  Created by Bruno Rocha on 15/07/16.
//  Copyright © 2016 Bruno Rocha. All rights reserved.
//

import UIKit
import CoreData

class WebViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var lblInfo:UILabel!
    @IBOutlet weak var myTableWeb : UITableView!
    
    var managedContext: NSManagedObjectContext!
    var contatoService : ContatoServices!
    var contatos = [ContatoClass]()
    var myActivityIndicator = UIActivityIndicatorView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lblInfo.hidden = true
        
        myActivityIndicator = UIActivityIndicatorView(frame: CGRectMake(0,0,50,50))
        myActivityIndicator.center = self.view.center
        //        myActivityIndicator.hidesWhenStopped = true
        myActivityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.Gray
        self.view.addSubview(myActivityIndicator)
        
        carregarDadosWeb()
    }
    
    func activityInSwitch(ligar:Bool){
        myActivityIndicator.hidesWhenStopped = true
        self.view.addSubview(myActivityIndicator)
        
        if (ligar){
            self.myActivityIndicator.startAnimating()
            UIApplication.sharedApplication().beginIgnoringInteractionEvents()
        }else{
            self.myActivityIndicator.stopAnimating()
            UIApplication.sharedApplication().endIgnoringInteractionEvents()
        }
    }
    
    func carregarDadosWeb(){
        activityInSwitch(true)
        managedContext = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
        contatoService = ContatoServices(context: managedContext)
        
        let urlWBS = "http://egcservice.com/webservices/contatos/listar_contatos.php"
        let session = NSURLSession.sharedSession()
        let urlObj = NSURL(string: urlWBS)!
        let myTask = session.downloadTaskWithURL(urlObj, completionHandler:
            {
                (location, response, error) -> Void in
                if(error == nil){
                    let objData = NSData(contentsOfURL: location!)
                    do{
                        let jsonData : NSDictionary = try NSJSONSerialization.JSONObjectWithData(objData!, options: NSJSONReadingOptions.MutableContainers) as! NSDictionary
                        let results = jsonData["contatos"] as! NSArray
                        
                        if (results.count > 0){
                            self.lblInfo.hidden = true
                            for result in results{ //x in 0 ... results.count-1{
                                //let res = results[x] as! NSDictionary
                                let nome = result["NOME"] as! String
                                let email = result["EMAIL"] as! String
                                let con = ContatoClass()
                                con.setNomeContato(nome)
                                con.setEmailContato(email)
                                self.contatos.append(con)
                            }
                        }else{
                            self.lblInfo.hidden = false
                            self.lblInfo.text = "Não foi encontrado Contatos salvos na web!"
                        }
                        
                        dispatch_async(dispatch_get_main_queue(), {
                            self.myTableWeb.reloadData()
                        })
                    }catch{
                        self.exibirAlert("Erro!", msg:"Erro ao listar os contatos", nameBtn:"Ok")
                    }
                }else {
                    self.exibirAlert("Erro!", msg:"Erro ao listar os contatos", nameBtn:"Ok")
                }
            })
        myTask.resume()
        activityInSwitch(false)
    }

    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return contatos.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell{
        let myCell = tableView.dequeueReusableCellWithIdentifier("myCellWeb", forIndexPath: indexPath)
        let contato = contatos[indexPath.row]
        
        let cNome = contato.getNomeContato()
        let cEmail = contato.getEmailContato()
        
        myCell.textLabel?.numberOfLines = 2
        //myCell.textLabel?.textAlignment = NSTextAlignment.Center
        myCell.textLabel?.lineBreakMode = NSLineBreakMode.ByWordWrapping
        myCell.imageView?.image = UIImage(named: "icon_cloud")
        myCell.textLabel!.text = cNome + "\n" + cEmail
        return myCell
    }
    
    func exibirAlert(titleAlert: String, msg: String, nameBtn:String){
        let myActionSheet : UIAlertController = UIAlertController(title: titleAlert, message: msg, preferredStyle:UIAlertControllerStyle.ActionSheet)
        let myAction : UIAlertAction = UIAlertAction(title: nameBtn, style: UIAlertActionStyle.Default, handler:nil)
        myActionSheet.addAction(myAction)
        self.presentViewController(myActionSheet, animated: true, completion: nil)
    }
    
    @IBAction func atualizarButton(){
        activityInSwitch(true)
        self.contatos = [ContatoClass]()
        carregarDadosWeb()
        activityInSwitch(false)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
